#Schema Logico-Relazionale:
#Prodotto(ID, Tipo(pescato, pesca), Nome, Costo, Sconto, Quantità rimasta, Descrizione, immagine, Numero ordini)
#Ricetta(Nome, Descrizione, Immagine)
#Ingrediente(Prodotto*, Ricetta*)
#Cliente(E-mail, password, nome, cognome)
#Carrello(Cliente*, Prodotto*, Quantità)
#Ordine(ID, Data di Acquisto, Prezzo Totale, Spedizione Rapida, Cliente*)
#Elemento Ordine(Prodotto*, Ordine*, Prezzo Corrente)

drop database if exists cyberfish_db;
create database cyberfish_db;
use cyberfish_db;

create table prodotto (
	id int primary key auto_increment,
    tipo int not null,
    nome char(32) not null,
    costo double not null,
    sconto int not null,
    quantita_rimasta int not null,
    descrizione text not null,
    immagine char(100) not null,
    numero_ordini int not null
);

create table ricetta (
	nome char(32) primary key,
    descrizione text not null,
    immagine char(100) not null
);

create table ingrediente (
	prodotto int,
    ricetta char(32),
    
    primary key (prodotto , ricetta),
    foreign key (prodotto) references prodotto(id) on delete cascade,
    foreign key (ricetta) references ricetta(nome) on delete cascade
);

create table utente (
	id int auto_increment primary key,
	email char(50) unique,
    password char(255) not null,
    nome char(24) not null,
    cognome char(24) not null,
    admin boolean not null
);

create table carrello (
	utente int,
    prodotto int,
    quantita int not null,
    
    primary key (utente, prodotto),
    foreign key (utente) references utente(id),
    foreign key (prodotto) references prodotto(id)
);

create table ordine (
	id int primary key auto_increment,
    data_acquisto date not null,
    prezzo_totale double not null,
    spedizione_rapida bool not null,
    utente int not null,
    nome_indirizzo char(255),
    email_indirizzo char(255),
    indirizzo char(255),
    citta_indirizzo char(50),
    CAP_indirizzo char(5),
    foreign key (utente) references utente(id)
    
);

create table elemento_ordine (
	prodotto int,
    ordine int,
    prezzo_corrente double not null,
    quantita_acquistata int not null,
    
    primary key (prodotto, ordine),
    foreign key (prodotto) references prodotto(id),
    foreign key (ordine) references ordine(id)
);



insert into prodotto values
(1,1,"Pesce San Pietro", 60.00,20,10,"Il pesce San Pietro è una varietà di pesce pregiato che vive in acqua salata e presenta carni molto saporite. Il pesce San Pietro è molto tenero e facile da pulire perché presenta poche lische. Il fatto che siano poche e molto grandi le rende facilmente individuabili, è perfetto quindi anche per i piatti dei più piccoli. Il pesce San Pietro ha un sapore ricco dell’alto valore nutrizionale.","images/pesce/pesceSanPietro.jpg",0),
(2,1,"Rombo",30.00,0,10,"Il rombo fa parte dei pesci chiamati piatti. La sua carne bianca è gustosa, leggera e povera di grassi. Altamente digeribile è anche ricco di minerali come lo iodio, il fosforo e il calcio. Il gusto del rombo è di gran carattere, è un pesce dal sapore così invitante che non ha bisogno di essere cucinato in modo troppo elaborato. Anche gli scarti del rombo, come le lische e la testa, sono ottimi se vuoi ottenere un saporito brodo di pesce per risotti e zuppe.","images/pesce/rombo.jpg",0),
(3,1,"Trota Bianca",8.00,0,10,"La trota bianca si presenta con una pelle iridescente e delle carni molto succose e chiare. Perfetta per preparazioni veloci e versatili è deliziosa sia in piastra che affumicata o marinata.","images/pesce/trotaBianca.jpg",0),
(4,1,"Ricciola",35.00,50,10,"La ricciola è un pesce azzurro dalle carni bianche e delicate, molto diffusa sulle tavole degli italiani e anche nella ristorazione più ricercata. La ricciola è un pesce che si presta, grazie alla consistenza della sua carne, sia a lavorazioni di mare pregiate sia a piatti semplici e sfiziosi come i carpacci oppure leggermente marinata con qualche goccia di limone.","images/pesce/ricciola.jpg",0),
(5,1,"Triglia di Sabbia",7.00,0,10," La ricciola è un pesce che si presta, grazie alla consistenza della sua carne, sia a lavorazioni di mare pregiate sia a piatti semplici e sfiziosi come i carpacci oppure leggermente marinata con qualche goccia di limone.","images/pesce/trigliaSabbia.jpg",0),
(6,1,"Branzino",15.00,0,10,"Il branzino, anche detto spigola, è uno dei pesci più amati per la nobiltà delle sue carni, sempre gustose e delicatissime. Si tratta di una specie molto diffusa nei nostri mari, da qui la sua fortuna sul mercato e in ristorazione. Questo pesce è ideale per ottenere ottimi piatti veloci e sfiziosi come il branzino al vapore, al cartoccio con patate, al sale, sotto forma di polpette di mare.","images/pesce/branzinoMediterraneo.jpg",0),
(7,1,"Sogliola",9.30,15,10,"La sogliola è tra i pesci più apprezzati e impiegati in cucina, per la facilità con cui si sfiletta e per la versatilità di preparazione. Si tratta di un pesce molto magro adatto a chi segue diete ipocaloriche e non vuole rinunciare al gusto.","images/pesce/sogliola.jpg",0),
(8,1,"Cefalo",3.50,0,10,"Il cefalo o muggine è un pesce molto diffuso in numerosi mari del mondo, incluso il nostro Mar Mediterraneo. Famoso anche per le sue deliziose uova con cui viene realizzata la pregiata bottarga di muggine, presenta delle carni delicate e squisite","images/pesce/cefalo.jpg",0),
(9,1,"Orata",15.50,0,10,"L’orata è un delizioso pesce molto abbondante nel mar Mediterraneo, tra i più stimati e apprezzati per le sue carni delicate e gustose. Si presenta tenera, leggera e di grande eleganza per la preparazione di secondi piatti. L’orata è deliziosa sia cucinata con qualche fetta di pomodoro in umido, che su un letto di patate al forno. Ti consigliamo di esaltare le sue carni con l’aroma del limone e stupire i tuoi ospiti portando in tavola il profumo del mare.","images/pesce/cefalo.jpg",0),
(10,2,"Piovra",29.90,0,10,"La piovra, chiamata anche polpo, è un mollusco con tentacoli molto evidenti che si presta a piatti semplici e sfiziosi. La piovra è ottima da gustare sia in insalata che in umido con patate, ma anche alla griglia. La piovra contiene proteine ad alto valore biologico considerate dai nutrizionisti tra le più sane in assoluto.","images/molluschi/piovra.jpg",0),
(11,2,"Vongole Veraci",19.90,5,10,"Le vongole veraci sono tra i molluschi prediletti dagli italiani. Dal sapore e profumo intenso riescono a rendere qualsiasi primo piatto una pietanza gustosa, arricchiscono i secondi di pesce, e possono essere una bontà assoluta anche consumate leggermente saltate in un sauté","images/molluschi/vongole.jpg",0),
(12,2,"Calamaro",8.50,0,10,"Il calamaro è tra i molluschi più pregiati e preferiti di sempre. La sua carne magra, delicata e gustosa è perfetta sia per le diete ipocaloriche che per numerose preparazioni in cucina. Si presta a ricette molto versatili e veloci, è delizioso sia fritto in una gustosa frittura all’italiana, che cotto al forno con pomodorini e pan grattato alle erbette aromatiche.","images/molluschi/calamaro.jpg",0),
(13,2,"Lupini dell'Adriatico",11.50,0,10,"Le vongole sono i frutti di mare più amati dagli italiani per il loro sapore intenso e caratteristico. Il profumo di mare che riescono a sprigionare, anche in cotture semplici e veloci, le rende uniche. Le vongole si prestano a numerose preparazioni come il sauté o in secondi di mare sfiziosi e profumati.","images/molluschi/lupini.jpg",0),
(14,2,"Seppia",7.50,0,10,"La seppia fa parte del grande mondo dei molluschi. La sua dimensione è variabile, ma il sapore e l’ottima consistenza della sua carne non cambia. La seppia è soda e gradevole, come se si sciogliesse in bocca, è ipocalorica e con un’elevata quantità di proteine ad alto valore biologico. Le seppie si valorizzano al massimo nel loro sapore sia cucinate alla griglia che utilizzate in sughi o risotti alla pescatora.","images/molluschi/seppia.jpg",0),
(16,3,"Gambero Rosso",11.90,10,10,"Il gambero rosso di Mazara è una specie pregiata che viene pescata nelle acque del nostro Mar Mediterraneo. Una delle caratteristiche principali è il suo colore rosso deciso e un sapore intenso delle carni in grado di rendere saporita qualsiasi pietanza. La carne soda del gambero rosso di Mazara è di color bianco e ha un sapore inconfondibile.","images/crostacei/gamberoRosso.jpg",0),
(17,3,"Aragosta",49.90,0,10,"L’aragosta è un crostaceo molto pregiato, dalla carne dolce e molto delicata. Il risultato migliore lo si ottiene cucinandola intera e ancora viva in acqua bollente, dopo averla pulita e lavata sotto acqua corrente. Essendo considerato un alimento allergizzante, ne è sconsigliato il consumo alle donne in stato di gravidanza, in allattamento e nel periodo dello svezzamento","images/crostacei/aragosta.jpg",0),
(18,3,"Scampi",17.90,0,10,"Gli scampi sono tra i crostacei più gustosi e pregiati al mondo. Il loro sapore li accomuna agli astici ma le loro dimensioni sono più ridotte e hanno una coda più morbida. Gli scampi presentano una carne soda e un guscio di colore rosato-bianco. Sono perfetti per le ricette di primi piatti di mare o anche alla piastra dove sprigionano al meglio il loro profumo prelibato.","images/crostacei/scampi.jpg",0),
(15,2,"Cozze", 9.80,0,10,"La cozza è un delizioso mollusco dal sapore deciso, la cui carne va assaporata cruda o senza eccessiva cottura. Le nostre cozze provengono dal Mar Mediterraneo (Sardegna, Puglia, Liguria) e gustandole proverai tutto il sapore della nostra Italia, ti basterà una spruzzata di limone e sentirai il mare. Le cozze si sposano bene con il prezzemolo in preparazioni al gratin con pepe e pane raffermo. Qualunque sia la tua ricetta, portare delle cozze in tavola fa subito festa.","images/molluschi/cozze.jpg",0),
(19,3,"Astice",29.90,20,10,"L’astice ha una carne delicata e molto gustosa, dal moderato apporto calorico. Si presta molto bene nella preparazione di ricette sfiziose e invitanti. Essendo considerato un alimento allergizzante, ne è sconsigliato il consumo alle donne in stato di gravidanza, in allattamento e nel periodo dello svezzamento.","images/crostacei/astice.jpg",0);

insert into ricetta values
("Pesce San Pietro in Padella", "descrizioneRicette/ricettaSanPietro.txt", "images/ricette/ricettaSanPietro.avif"),
("Saute di Cozze e Vongole","descrizioneRicette/ricettaCozzeVongole.txt","images/ricette/ricettaCozzeVongole.avif"),
("Frittura di Gamberi e Calamari","descrizioneRicette/ricettaGamberiCalamari.txt","images/ricette/ricettaGamberiCalamari.png"),
("Spigola al cartoccio", "descrizioneRicette/ricettaSpigola.txt","images/ricette/ricettaSpigola.png"),
("Rombo al Forno", "descrizioneRicette/ricettaRombo.txt","images/ricette/ricettaRombo.avif"),
("Linguine ai Tre Crostacei", "descrizioneRicette/ricetta3Crostacei.txt","images/ricette/ricetta3Crostacei.png");


insert into ingrediente values
(1,"Pesce San Pietro in Padella"),
(15,"Saute di Cozze e Vongole"),
(11,"Saute di Cozze e Vongole"),
(12,"Frittura di Gamberi e Calamari"),
(16,"Frittura di Gamberi e Calamari"),
(6,"Spigola al cartoccio"),
(2,"Rombo al Forno"),
(17,"Linguine ai Tre Crostacei"),
(18,"Linguine ai Tre Crostacei"),
(19,"Linguine ai Tre Crostacei");