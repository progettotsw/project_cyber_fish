<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>

        <%@include file="WEB-INF/includes/commonHead.html"%>

        <link rel="stylesheet" type="text/css" href="css/login.css">

        <script src="scripts/loginValidate.js"></script>
        <script>
            function showPass() {
                let pass = document.getElementById("passwordField");

                if(pass.type === "password")
                    pass.type = "text";
                else
                    pass.type = "password";
            }
        </script>

        <title>Cyberfish - LOGIN</title>
    </head>
    <body class="" style="margin: 0">
        <%@include file="WEB-INF/includes/header.jsp"%>

        <form action="AuthServlet" method="post" onsubmit="return validate()" class="formContainer">
                <input type="hidden" name="type" value="login">

                <label for="emailField"><b>E-mail</b></label> <br>
                <input type="text" id="emailField" name="email" placeholder="E-mail" required> <br>

                <label for="passwordField"><b>Password</b></label> <br>
                <input type="password" id="passwordField" name="password" placeholder="Password" required> <br>

                <input type="checkbox" id="showPassword" onclick="showPass()">
                <label for="showPassword">Mostra Password</label>

                <button type="submit"> Login </button>
                <%
                    String visible = "hidden";
                    String outcome = (String) request.getAttribute("outcome");
                    if(outcome != null)
                        if(outcome.equals("false"))
                            visible = "visible";
                %>

                <span>Non sei <a href="registrazione.jsp">registrato?</a></span> <br>
                <div class="divider"></div>
                <span style="visibility: <%=visible%>" id="validation_error_msg">
                    C'è stato un errore con l'autenticazione. Controllare che e-mail e password inserite siano corrette.
                </span>
        </form>

        <%@include file="WEB-INF/includes/footer.html"%>
    </body>
</html>
