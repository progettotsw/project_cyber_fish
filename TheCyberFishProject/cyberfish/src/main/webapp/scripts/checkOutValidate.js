function validate() {
    let nome = $("#nome").val();
    let emailPattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    let email = $("#email").val();
    let indirizzo = $("#indirizzo").val();
    let citta = $("#citta").val();
    let capPattern = /^[0-9]{5}$/;
    let cap = $("#CAP").val();

    let ccnumPattern = /^(([0-9]{4}[- ]){3}[0-9]{4})$/;
    let ccnum = $("#ccnum").val();
    let cvvPattern = /^[0-9]{3}$/;
    let cvv = $("#cvv").val();

    if(nome.length > 255) {
        $("#errorMsg").html("Il nome inserito è troppo lungo. Non superare i 255 caratteri.").css("visibility", "visible");
        return false;
    }
    else if(email.length > 255) {
        $("#errorMsg").html("La e-mail inserita è troppo lunga. Non superare i 255 caratteri.").css("visibility", "visible");
        return false;
    }
    else if(!emailPattern.test(email)) {
        $("#errorMsg").html("L' e-mail inserita è errata o non supportata.").css("visibility", "visible");
        return false;
    }
    else if(indirizzo.length > 255) {
        $("#errorMsg").html("L'indirizzo inserito è troppo lungo. Non superare i 255 caratteri.").css("visibility", "visible");
        return false;
    }
    else if(citta.length > 50) {
        $("#errorMsg").html("La città inserita ha un nome troppo lungo. Non superare i 50 caratteri.").css("visibility", "visible");
        return false;
    }
    else if(!capPattern.test(cap)) {
        $("#errorMsg").html("Il CAP inserito è errato.").css("visibility", "visible");
        return false;
    }
    else if(!ccnumPattern.test(ccnum)) {
        $("#errorMsg").html("Il numero di carta di credito inserito è formattato in maniera errata.").css("visibility", "visible");
        return false;
    }
    else if(!cvvPattern.test(cvv)) {
        $("#errorMsg").html("Il CVV inserito non è composto da tre cifre.").css("visibility", "visible");
        return false;
    }

    return true;
}