let id = "";
let cardType = -1;
function setElementID(newId){
    id=newId;
}
function setCardType(newType){
    cardType=newType;
}
function createCards(result){
    if(cardType === 0)
    {
        createProductCards(result);
    }
    else
    {
        createRecipeCards(result);
    }
}


function createProductCards(result) {
    let products = result;
    document.getElementById(id).innerHTML = "";
    let cardHTML = "";

    for(let i = 0; i < products.length; i++) {
        cardHTML += "<div class=\"card\">"
        cardHTML += "<a href=\"ProductPage?id="+ products[i]["id"] + "\">" + "<img src=\"" + products[i]["immagine"] + "\" alt=\"" + products[i]["nome"] + "\">" + "</a>";

        cardHTML += "<h1 class=\"productName\">" + products[i]["nome"] + "</h1>";

        cardHTML += "<span class=\"price\">" + (products[i]["costo"] - products[i]["costo"] * products[i]["sconto"] / 100).toFixed(2) + "&euro;</span>";
        if(products[i]["sconto"] > 0) {
            cardHTML += "<span class=\"realPrice\">" + (products[i]["costo"]).toFixed(2) + "&euro;</span>" + "<span class=\"DiscountTag\">" + products[i]["sconto"] + "%</span>";
        }
        cardHTML += "<button id =\"button" + products[i]["id"] + "\" onclick=\"addToCartButton(" + products[i]["id"] + ", 'button" + products[i]["id"] + "')\"> Aggiungi al carrello </button> </div>";
    }

    document.getElementById(id).innerHTML = cardHTML;
}

function createRecipeCards(result){
   let recipes = result;
    document.getElementById(id).innerHTML = result;
    let cardHTML = "";

    for(let i = 0; i< recipes.length; i++) {
        cardHTML += "<div class=\"card\">"
        cardHTML += "<a href='RecipePage?name=" + recipes[i]["nome"] + "'>" + "<img src=\"" + recipes[i]["immagine"] + "\" alt=\"" + recipes[i]["nome"] + "\">" + "</a>";
        cardHTML += "<h1 class=\"productName\">" + recipes[i]["nome"] + "</h1>" + "</div>";
    }
    document.getElementById(id).innerHTML = cardHTML;
}

