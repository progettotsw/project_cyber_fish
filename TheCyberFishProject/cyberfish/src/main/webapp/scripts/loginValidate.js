function validate() {
    const email = $("#emailField").val();
    const emailPattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    const password = $("#passwordField").val();
    const passwordPattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+/

    if(!(emailPattern.test(email))) {
        $("#validation_error_msg").html("L' e-mail inserita è errata o non supportata.").css("visibility", "visible");
        return false;
    }
    else if(!(passwordPattern.test(password))) {
        $("#validation_error_msg").html("La password inserita contiene caratteri non supportati. " +
            "<br> Utilizzare esclusivamente: " +
            "<ul>" +
                "<li>Lettere</li>" +
                "<li>Numeri</li>" +
                "<li>I caratteri .!#$%&'*+/=?^_`{|}~-</li>" +
            "</ul>")
            .css("visibility", "visible");
        return false;
    }
    else if(password.length < 8) {
        $("#validation_error_msg").html("La password inserita è troppo corta (min 8 caratteri)").css("visibility", "visible");
        return false;
    }
    else if(password.length > 32) {
        $("#validation_error_msg").html("La password inserita è troppo lunga (max 32 caratteri)").css("visibility", "visible");
        return false;
    }
    else
        return true;
}