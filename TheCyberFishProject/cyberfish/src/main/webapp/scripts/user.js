function editName(fieldValue){
    document.getElementById("nameTextArea").innerHTML = "<input id =\"nameField\" type=\"text\" value=\"" + fieldValue + "\">" +
        "<button onclick=\"onEnter('nameField',1,'nameTextArea')\">OK</button>"

}
function editSurname(fieldValue){
    document.getElementById("surnameTextArea").innerHTML = "<input id =\"surnameField\" type=\"text\" value=\"" + fieldValue + "\">" +
        "<button onclick=\"onEnter('surnameField',2,'surnameTextArea')\">OK</button>"

}
function editEmail(fieldValue){
    document.getElementById("emailTextArea").innerHTML = "<input id =\"emailField\" type=\"email\" value=\"" + fieldValue + "\">" +
        "<button onclick=\"onEnter('emailField',3,'emailTextArea')\">OK</button>"

}
function editPassword(){
    document.getElementById("passwordTextArea").innerHTML = "<input id =\"passwordField\" type=\"password\">" +
        "<button onclick=\"onEnter('passwordField',4,'passwordTextArea')\">OK</button>"

}


function onEnter(textField, type, area){
    let correctInput = true;
    let value = document.getElementById(textField).value;
    const emailPattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    const passwordPattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+/;

    switch(type) {
        case 1:
            correctInput = (value.length <= 24);
            break;
        case 2:
            correctInput = (value.length <= 24);
            break;
        case 3:
            correctInput = (emailPattern.test(value));
            break;
        case 4:
            correctInput = (passwordPattern.test(value) && value.length >= 8 && value.length <= 32);
            break;
        default:
            correctInput = false;
    }

    if(correctInput) {
        $.post(
            "UserUpdate",
            {
                type: type,
                value: value
            }
        ).done(function(data){
            if(type === 4){
                document.getElementById(area).innerHTML = "********";
            }
            else{
                document.getElementById(area).innerHTML = value;
            }
        });
    }
}


function createProductTable(result){
        let products = result;
        sessionStorage.setItem("products",products);
        let productTableHTML = "";
        document.getElementById("productsTable").innerHTML = "";
        for(let i = 0; i < products.length; i++){
            productTableHTML += "<tr id=" + products[i]["id"] + ">";
            productTableHTML += "<td>" + "ID:" + products[i]["id"] + "</td>";
            productTableHTML += "<td>" + products[i]["nome"] + "</td>";
            productTableHTML += "<td>" +"<button onclick=\"editProduct("+ products[i]["id"]+")\"> Modifica</button>" + "</td>";
            productTableHTML += "<td>" +"<button onclick=\"removeProduct(" + products[i]["id"] +")\"> Elimina</button>" + "</td>";
            productTableHTML += "</tr>";
        }
        productTableHTML += "<tr>" + "<td><button onclick=\"addProduct()\">Aggiungi Prodotto</button></td>" + "</tr>";
        document.getElementById("productsTable").innerHTML = productTableHTML;
}


function removeProduct(id){
    $.ajax({
        url: "ProductEdit?type=2&id=" + id,
        success: function (){
            document.getElementById(id).innerHTML = "";
        }
    })
}
function editProduct(id){
    window.location.href = "ProductPage?edit=true&id=" + id;
}

function addProduct(){
    window.location.href="productPageCreator.jsp";
}