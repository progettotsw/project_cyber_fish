let itemsAboveStock = 0;
let itemTracker = {};

function setItemsAboveStock(n) {
    itemsAboveStock = n;
}

function addToCartButton(prodId, buttonId, opacity) {
    $.get("CartHandler",
        {
            mode: "add",
            id: prodId
        },
        function(){
            let button = $("#" + buttonId);
            if(opacity === 1) {
                button.animate({
                    opacity: "0.5"
                }, 100);
                button.animate({
                    opacity: "1"
                }, 100);
            }
            else {
                button.animate({
                    width: "-=1rem"
                }, 50);
                button.animate({
                    width: "+=1rem"
                }, 50);
            }

        }
    );
}

function addToCart(id) {
    $.get("CartHandler",
        {
            mode: "add",
            id: id
        },
        function(result) {
            $("#quantityOf" + id).html(result["newQuantity"]);
            $("#totalPriceOf" + id).html((result["newQuantity"] * result["itemPrice"]).toFixed(2) + "&euro;");
            $("#cartPrice").html((parseFloat($("#cartPrice").html()) + result["itemPrice"]).toFixed(2));
            if(result["newQuantity"] > result["stockQuantity"]) {
                $("#aboveStock" + id).show();
                if(itemTracker["item" + id] === undefined || itemTracker["item" + id] === false) {
                    itemTracker["item" + id] = true;
                    itemsAboveStock ++;
                }
            }
            else {
                $("#aboveStock" + id).hide();
            }
        }
    );
}

function subFromCart(id) {
    $.get("CartHandler",
        {
            mode: "sub",
            id: id
        },
        function(result) {
            $("#cartPrice").html((parseFloat($("#cartPrice").html()) - result["itemPrice"]).toFixed(2));
            if(result["remove"]) {
                document.getElementById(id.toString()).remove();
                if(itemTracker["item" + id] === true){
                    itemTracker["item" + id] = undefined;
                    itemsAboveStock --;
                }

                if(document.getElementsByClassName("item").length === 0) {
                    $("#items").html("<h1> Il carrello è vuoto :( </h1>");
                    $("#cartPrice").html("0.00");
                }
            }
            else {
                $("#quantityOf" + id).html(result["newQuantity"]);
                $("#totalPriceOf" + id).html((result["newQuantity"] * result["itemPrice"]).toFixed(2)+ "&euro;");
                if(result["newQuantity"] > result["stockQuantity"]) {
                    $("#aboveStock" + id).show();
                }
                else {
                    $("#aboveStock" + id).hide();
                    if(itemTracker["item" + id] === true){
                        itemsAboveStock --;
                        itemTracker["item" + id] = false;
                    }
                    else if(itemTracker["item" + id] === undefined) {
                        itemTracker["item" + id] = false;
                    }
                }
            }
        }
    );
}

function remFromCart(id) {
    $.get("CartHandler",
        {
            mode: "remove",
            id: id
        },
        function() {
            let totalPriceOfEntry = parseFloat(document.getElementById("totalPriceOf" + id).innerHTML);
            document.getElementById("cartPrice").innerHTML = (parseFloat(document.getElementById("cartPrice").innerHTML) - totalPriceOfEntry).toFixed(2);
            document.getElementById(id).remove();
            if(itemTracker["item" + id] === true){
                itemTracker["item" + id] = undefined;
                itemsAboveStock --;
            }
            if(document.getElementsByClassName("item").length === 0) {
                document.getElementById("items").innerHTML = "<h1> Il carrello è vuoto :( </h1>";
                document.getElementById("cartPrice").innerHTML = "0.00";
            }
        }
    );
}
function createOrder(isLoggedIn){

    if(itemsAboveStock > 0) {
        alert("Alcuni degli oggetti nel tuo carrello superano la quantità attualmente presente nelle nostre scorte!");
        return;
    }

    if(!isLoggedIn){
        if(confirm("Devi essere loggato per creare un ordine! Vuoi effettuare il login?") === true){
            window.location.replace("login.jsp");
        }
    }
    else{
        window.location.replace("checkOut.jsp")
    }
}

