<%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <html>
    <head>
        <title>Cyberfish - AGGIORNA PRODOTTO</title>
        <link rel="stylesheet" type="text/css" href="css/editPage.css">
        <%@include file="WEB-INF/includes/commonHead.html"%>
    </head>
    <body>
        <%@include file="WEB-INF/includes/header.jsp"%>
        <h2>Crea il Prodotto:</h2>
        <form method="post" action="ProductCreator" class="container" enctype='multipart/form-data'>
            <div class="rowContainer">
                <label for="immagine">Immagine:</label>
                <input type="file" name="immagine" id="immagine">
                <label for="tipo">Tipo: </label>
                <select name="tipo" id="tipo">

                    <option value="1">Pesce</option>
                    <option value="2">Molluschi</option>
                    <option value="3">>Crostacei</option>
                </select>

                <label for="nome">Nome: </label>
                <input type="text" name="nome" id="nome">
                <label for="descrizione">Descrizione: </label>
                <textarea name="descrizione" id="descrizione" cols="30" rows="10"></textarea>
                <label for="costo">Costo: </label>
                <input type="number" step="0.01" id="costo" name="costo">
                <label for="sconto">Sconto: </label>
                <input type="number" id="sconto" name="sconto" max="100" min="0">
                <label for="quantita">Quantit&agrave; Rimasta:</label>
                <input type="number" id="quantita" name="quantita" min="0">
                <input type="hidden" id="numeroOrdini" name="numeroOrdini">
                <input type="hidden" id="pathOriginale" name="pathOriginale">
                <input type="hidden" id="id" name="id">
                <input type="submit" value="Inserisci Prodotto">
            </div>


        </form>
        <%@include file="WEB-INF/includes/footer.html"%>
    </body>
</html>
