<%@ page import="tsw.project.cyberfish.model.ClienteBean" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/includes/commonHead.html"%>

        <link rel="stylesheet" type="text/css" href="css/index.css">

        <title>Cyberfish - HOME</title>
    </head>
    <body>
        <%@include file="WEB-INF/includes/header.jsp"%>

        <%
            ClienteBean cliente =(ClienteBean) session.getAttribute("cliente");

            if(cliente != null)
            {
                String name = cliente.getNome();
                %> <h1> Welcome to Cyberfish, <%= name %> </h1> <%
            }
            else {
        %>
            <h1> Welcome to Cyberfish!  </h1>
        <%
                }
        %>

        <div class="carousel-container">
            <div class="slide fade">
                <div class="numbertext">1 / 4</div>
                <img src="images/home/Pescheria1.jpeg" alt="pesce" style="width:100%">
            </div>

            <div class="slide fade">
                <div class="numbertext">2 / 4</div>
                <img src="images/home/pescatore2.jpeg" alt="pesce" style="width:100%">
            </div>

            <div class="slide fade">
                <div class="numbertext">3 / 4</div>
                <img src="images/home/barca1.jpeg" alt="pesce" style="width:100%">
            </div>

            <div class="slide fade">
                <div class="numbertext">4 / 4</div>
                <img src="images/home/barca2.jpeg" alt="pesce" style="width:100%">
            </div>
        </div>

        <br>
        <div style="text-align:center">
            <span class="dot"></span>
            <span class="dot"></span>
            <span class="dot"></span>
            <span class="dot"></span>
        </div>

        <div class="divider"> Prodotti più Venduti</div>


        <div class="cardContainer" id="topPescato"></div>
        <%@include file="WEB-INF/includes/footer.html"%>

        <script>
            let slideIndex = 0;
            carousel();
            productsInit();

            function carousel() {
                let i;
                let slides = document.getElementsByClassName("slide");
                let dots = document.getElementsByClassName("dot");
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                slideIndex++;
                if (slideIndex > slides.length) {slideIndex = 1}
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex-1].style.display = "block";
                dots[slideIndex-1].className += " active";
                setTimeout(carousel, 5000);
            }

            function productsInit(){
                setCardType(0);
                setElementID("topPescato");
                $.ajax({
                    url:"ProductsGetter?type=0&top=6",
                    success: function(result){
                        createCards(result);
                    }
                })
            }
        </script>

    </body>
</html>