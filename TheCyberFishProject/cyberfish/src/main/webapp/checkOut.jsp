<%@ page import="tsw.project.cyberfish.model.ClienteBean" %>
<%@ page import="tsw.project.cyberfish.model.CartEntry" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Cyberfish - CHECKOUT</title>
        <link rel="stylesheet" type="text/css" href="css/checkOut.css">

        <script src="scripts/checkOutValidate.js"></script>

        <%@include file="WEB-INF/includes/commonHead.html"%>
    </head>
    <body>
        <%@include file="WEB-INF/includes/header.jsp"%>
        <%
            ClienteBean user = (ClienteBean) session.getAttribute("cliente");
            int id = user.getId();
            List<CartEntry> cart = (List<CartEntry>) session.getAttribute("carrello");
            double totalPrice = 0;
            for(CartEntry entry: cart) {
                totalPrice += (entry.getProdotto().getCosto() * 100 - (entry.getProdotto().getCosto() * entry.getProdotto().getSconto())) * entry.getEntry().getQuantita();
            }
        %>

        <form action="CheckOutValidator" class="formContainer" method="post" onsubmit="return validate()">
            <div class="rowContainer">
                <h2> Indirizzo di Spedizione</h2>
                <input type="hidden" name="userID" value="<%=id%>">
                <label for="nome" class="fa-solid fa-user"><span> Nome Completo</span></label>
                <input type="text" id="nome" name="nome" placeholder="Mario Rossi" required>
                <label for="email" class="fa-solid fa-envelope"><span> Email</span></label>
                <input type="text" id="email" name="email" placeholder="mariorossi@gmail.com" required>
                <label for="indirizzo" class="fa-solid fa-address-card"><span> Indirizzo</span></label>
                <input type="text" id="indirizzo" name="indirizzo" placeholder="Via Roma 13" required>
                <label for="citta" class="fa-solid fa-institution"> <span> Citt&agrave;</span> </label>
                <input type="text" id="citta" name="citta" required>
                <label for="CAP"><span>CAP</span></label>
                <input type="text" name="CAP" id="CAP" placeholder="83100" required>
            </div>
            <div class="rowContainer">
                <h2>Pagamento</h2>
                <label for="cname" class="fa-solid fa-signature"><span> Nome sulla Carta</span></label>
                <input type="text" id="cname" name="nomeCarta" placeholder="Mario Rossi" required>
                <label for="ccnum" class="fa-solid fa-credit-card"><span> Numero di Carta</span></label>
                <input type="text" id="ccnum" name="numeroCarta" placeholder="1111-2222-3333-4444" required>
                <label for="expmonth" class="fa-solid fa-calendar"><span> Scadenza</span></label>
                <input type="month" id="expmonth" name="meseScadenza" placeholder="Agosto" required>
                <label for="cvv" class="fa-solid fa-shield-halved"><span> CVV</span></label>
                <input type="text" id="cvv" name="cvv" placeholder="410" required>
                <div class="deliveryContainer">
                    <label for="delivery" class="fa-solid fa-truck"><span> Spedizione Rapida</span></label>
                    <input type="checkbox" name="spedizioneRapida" id="delivery" value="true" onchange="setFastDelPrice()">

                </div>
                <span>Totale: <span id="totalPrice"><%=String.format(Locale.US, "%.2f", totalPrice / 100)%></span>&euro;</span>
            </div>

            <input type="submit" value="Ordina" class="orderButton"> <br>
            <span id="errorMsg" style="visibility: hidden; text-align: center"></span>
        </form>

        <script>
            let date = new Date();
            let month = date.getMonth() + 1;
            let year = date.getFullYear();

            if(month < 10){
                date = year + "-0" + month;
            }else{
                date = year + "-" + month;
            }
            document.getElementById("expmonth").setAttribute("min",date);
            function setFastDelPrice() {
                const baseTotal = <%=Math.ceil(totalPrice)%> / 100;
                let totalPrice = parseFloat($("#totalPrice").text());
                let fastDelivery = document.getElementById("delivery").checked;

                if(fastDelivery && totalPrice === baseTotal) {
                    totalPrice += 5;
                }
                else if(!fastDelivery && totalPrice > baseTotal) {
                    totalPrice -= 5;
                }

                $("#totalPrice").text(totalPrice.toFixed(2));
            }
        </script>

        <%@include file="WEB-INF/includes/footer.html"%>
    </body>
</html>
