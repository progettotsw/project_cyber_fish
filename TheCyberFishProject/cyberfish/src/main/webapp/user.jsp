<%@ page import="tsw.project.cyberfish.model.ClienteBean" %>
<%@ page import="tsw.project.cyberfish.model.OrdineBean" %>
<%@ page import="java.util.List" %>
<%@ page import="tsw.project.cyberfish.model.ElementoOrdineBean" %>
<%@ page import="java.util.Locale" %>
<%@ page import="tsw.project.cyberfish.model.ProdottoBean" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    ClienteBean cliente = (ClienteBean) session.getAttribute("cliente");
    String nome = cliente.getNome();
    String cognome = cliente.getCognome();
    String email = cliente.getEmail();
    boolean isAdmin = cliente.isAdmin();

%>
<html>
    <head>
        <%@ include file="WEB-INF/includes/commonHead.html"%>
        <script src="scripts/user.js"></script>
        <link rel="stylesheet" type="text/css" href="css/user.css">
        <title>Cyberfish - UTENTE</title>
        <script>
            if(<%=isAdmin%>)
            $.ajax({
                url: "ProductsGetter?top=0&type=0",
                success: function (result){
                    createProductTable(result);
                }

            })
        </script>
    </head>
    <body>
        <%@ include file="WEB-INF/includes/header.jsp"%>
        <h1>Terminale Utente</h1>
        <table>
            <tr>
                <th>Attributo</th>
                <th>Valore</th>
                <th>Azione</th>
            </tr>
            <tr>
                <td>E-mail</td>
                <td id="emailTextArea"><%=email%></td>
                <td><button onclick="editEmail('<%=email%>')">Modifica</button></td>
            </tr>
            <tr>
                <td>Nome</td>
                <td id="nameTextArea"><%=nome%></td>
                <td><button onclick="editName('<%=nome%>')">Modifica</button></td>
            </tr>
            <tr>
                <td>Cognome</td>
                <td id="surnameTextArea"><%=cognome%></td>
                <td><button onclick="editSurname('<%=cognome%>')">Modifica</button></td>
            </tr>
            <tr>
                <td>Password</td>
                <td id="passwordTextArea">********</td>
                <td><button onclick="editPassword()">Modifica</button></td>
            </tr>

        </table>
        <%
            boolean noOrders = (boolean) request.getAttribute("noOrders");
            if(noOrders) {
        %>
        <h1>Non hai ancora effettuato Ordini :(</h1>
        <%
        } else {
            List<OrdineBean> orders = (List<OrdineBean>) request.getAttribute("orders");
            List<ElementoOrdineBean> entries = (List<ElementoOrdineBean>) request.getAttribute("entries");
            List<ProdottoBean> products = (List<ProdottoBean>) request.getAttribute("products");
        %>
        <h1>Ordini</h1>
        <table>
            <%

                for(OrdineBean order: orders) {
            %>
            <tr>
                <th>Numero ordine: <%=order.getId()%></th>
                <th>Data di acquisto: <%=order.getDataAcquisto().toLocalDate()%></th>
                <th>Totale speso: <%=String.format(java.util.Locale.US, "%.2f", order.getPrezzoTotale())%> &euro;</th>
            </tr>
            <%
                for(ElementoOrdineBean entry: entries) {
                    if(entry.getOrdine() == order.getId()) {
                        for(ProdottoBean product: products) {
                            if(product.getId() == entry.getProdotto()) {
            %>
            <tr>
                <td><%=product.getNome()%></td>
                <td>Quantit&agrave; acquistata: <%=entry.getQuantitaAcquistata()%></td>
                <td>Prezzo di acquisto: <%=String.format(Locale.US, "%.2f", entry.getPrezzoCorrente())%> &euro;</td>
            </tr>
            <%
                                    break;
                                }
                            }
                        }
                    }
                }
            %>
        </table>
        <%
            }
        %>

        <% if(isAdmin)
        {
            %>
                <h1>Pannello Admin</h1>
                <table id="productsTable">
                </table>
                <div id="editProducts"></div>
            <%
        } %>



        <%@include file="WEB-INF/includes/footer.html"%>
    </body>
</html>