<%@ page import="tsw.project.cyberfish.model.ProdottoBean" %>
<%@ page import="java.util.Locale" %><%--
  Created by IntelliJ IDEA.
  User: Iacopo
  Date: 10/07/2023
  Time: 17:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="WEB-INF/includes/commonHead.html"%>
    <script src="scripts/cards.js"></script>
    <link rel="stylesheet" type="text/css" href="css/productPage.css">
    <title>Cyberfish - PRODOTTO</title>

</head>
<body>
    <%@ include file="WEB-INF/includes/header.jsp"%>
    <%
        ProdottoBean p = (ProdottoBean) request.getAttribute("prodottoBean");
        String nome = p.getNome();
        String descrizione = p.getDescrizione();
        String immagine = p.getImmagine();
        Double costo = p.getCosto();
        int sconto = p.getSconto();
        int quantitaRimasta = p.getQuantitaRimasta();
        int tipo = p.getTipo();
        boolean isOnDiscount = sconto != 0;
        double costoReale = costo*(100-sconto)/100;

    %>
    <main>
        <div class="container">
            <div class="imageContainer">
                <img class="productImage" src=<%=immagine%> alt="<%=nome%>">
            </div>

            <div class="descriptionContainer">
                <div class="description">
                    <%
                        if(tipo == 1){
                            %>
                                <span id="productTag">Pesce</span>
                            <%
                        }
                        else if(tipo == 2){
                            %>
                                <span id="productTag">Molluschi</span>
                            <%

                        }
                        else if(tipo == 3){
                            %>
                                <span id="productTag">Crostacei</span>
                            <%

                        }
                    %>

                    <h1 id="productPageName"><%= nome %></h1>
                    <div class="test">
                        <button class="addToCartButton" id="addToCart" onclick="addToCartButton(<%=p.getId()%>, 'addToCart', 1)"> Aggiungi al Carrello</button>
                        <div class="priceContainer">
                        <%
                            if(isOnDiscount) {
                        %>
                        <span class="discount"> <%=sconto%>%</span><span class="realProductPagePrice"> <%=String.format(Locale.US, "%.2f",costoReale)%>&euro; </span> <br>
                        <span class="productPagePriceText">Costo originale: <span class="productPagePrice"><%=String.format(Locale.US, "%.2f",costo)%>&euro;</span></span>
                        <%
                        }
                        else{
                        %>
                        <span class="realProductPagePrice"> <%=String.format("%.2f",costoReale)%>&euro;</span>
                        <%
                            }
                        %>
                    </div>
                    </div>
                    <p id="productDescription"><%=descrizione%></p>
                    <p class="productDisclaimer"> Il prezzo si riferisce ad un 800 g di prodotto non pulito.</p>
                </div>


            </div>
        </div>
        <div class="separator">Prodotti suggeriti:</div>
        <div class="cardContainer" id="topPescato"></div>
        <script>
            function productsInit() {
                setCardType(0);
                setElementID("topPescato");
                $.ajax({
                    url: "ProductsGetter?type=0&top=5",
                    success: function (result) {
                        createCards(result);
                    }
                })
            }
            productsInit();
        </script>
    </main>
    <%@ include file="WEB-INF/includes/footer.html"%>
</body>
</html>
