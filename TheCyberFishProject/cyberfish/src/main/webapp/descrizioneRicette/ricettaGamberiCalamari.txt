Ingredienti:

1. Calamari medi - 2 kg.
2. Gamberi - 1 kg. 
3. Farina di grano duro o semola q.b.
4. Olio di semi per frittura q.b.
5. Sale q.b.
6. Fettine di Limone q.b.

Pulite i calamari, rimuovete le ali e tagliateli ad anelli. Riunite i calamari in un sacchetto di plastica, unite qualche cucchiaio di farina di grano duro rimacinata e scuotete il tutto cosicchè gli anelli di calamaro siano completamente ricoperti dalla farina. Fate la stessa cosa con i gamberi rosa prestando più attenzione per non romperli.
Scaldate abbondante olio per fritture. Mentre l'olio si scalda, prendete un colino o un setaccio, rovesciatevi dentro i calamari e fate in modo da rimuovere la farina in eccesso.
Procedete allo stesso modo anche con i gamberi. Tuffate i calamari nell'olio bollente e lasciateli cuocere pochi minuti, giusto il tempo che si dorino.
Prelevate i calamari con un ragno o un cucchiaio forato ed adagiateli su fogli di carta paglia o di carta assorbente affinché s'asciughino dall'olio in eccesso.
Friggete, ora, anche i gamberetti, meglio sarebbe se i calamari e i gamberetti friggessero contemporaneamente in padelle diverse.
Salate il fritto e servitelo ancora caldo accompagnandolo con fettine di limone.