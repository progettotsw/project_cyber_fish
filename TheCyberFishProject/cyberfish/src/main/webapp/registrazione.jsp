<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <%@include file="WEB-INF/includes/commonHead.html"%>

        <link rel="stylesheet" type="text/css" href="css/login.css">

        <script src="scripts/registerValidate.js"></script>
        <script>
            function showPass() {
                let pass = document.getElementById("passwordField");
                let repPass = document.getElementById("repeatPasswordField");

                if(pass.type === "password")
                    pass.type = "text";
                else
                    pass.type = "password";

                if(repPass.type === "password")
                    repPass.type = "text";
                else
                    repPass.type = "password";
            }
        </script>
        <title>Cyberfish - REGISTRAZIONE</title>
    </head>
    <body style="margin: 0">
        <%@include file="WEB-INF/includes/header.jsp"%>

        <form action="AuthServlet" method="post" class="formContainer" onsubmit="return validate()">
            <input type="hidden" name="type" value="register">

            <label for="emailField">Indirizzo e-mail:</label> <br>
            <input type="text" name="email" id="emailField" placeholder="E-mail" required> <br>

            <label for="passwordField">Password:</label> <br>
            <input type="password" name="password" id="passwordField" placeholder="Password"
                   title="La password deve essere di almeno 8 caratteri e massimo 32 e può contenere caratteri speciali" required> <br>

            <label for="repeatPasswordField">Ripeti password:</label> <br>
            <input type="password" name="repeatPassword" id="repeatPasswordField" placeholder="Password"
                   title="Ripetere la password scritta al campo precedente" required> <br>

            <input type="checkbox" id="showPassword" onclick="showPass()"> <br>
            <label for="showPassword">Mostra Password</label> <br>
            <div class="divider"></div>
            <label for="nameField">Nome:</label> <br>
            <input type="text" name="name" id="nameField" placeholder="John" required> <br>

            <label for="surnameField">Cognome:</label> <br>
            <input type="text" name="surname" id="surnameField" placeholder="Fish" required> <br>

            <span>Hai già un account? <a href="login.jsp">Accedi</a> </span> <br>
            <button type="submit"> Registrati </button>


            <%
                String visible = "hidden";
                String outcome = (String) request.getAttribute("outcome");
                if(outcome != null)
                    if(outcome.equals("false"))
                        visible = "visible";
            %>

            <span style="visibility: <%=visible%>" id="validation_error_msg">
                C'è stato un errore nella creazione del tuo account. Controlla che i dati inseriti siano corretti e riprova.
            </span> <br>


        </form>
        <%@include file="WEB-INF/includes/footer.html"%>
    </body>
</html>
