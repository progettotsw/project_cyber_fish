<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="WEB-INF/includes/commonHead.html"%>
        <title>Cyberfish - RICETTE</title>
    <link rel="stylesheet" type="text/css" href="css/productCard.css">
    <style>
        .card img{
            width: 300px;
            height: 200px;
        }
    </style>
    <script src="scripts/cards.js"></script>
</head>
<body>
    <%@include file="WEB-INF/includes/header.jsp"%>
    <div class="cardContainer" id="recipeContainer"></div>
    <script>
        function recipesInit()
        {
            setCardType(1);
            setElementID("recipeContainer");
            $.ajax({
                url: "RecipesGetter",
                success: createCards
            })
        }
        recipesInit();
    </script>
    <%@include file="WEB-INF/includes/footer.html"%>
</body>
</html>
