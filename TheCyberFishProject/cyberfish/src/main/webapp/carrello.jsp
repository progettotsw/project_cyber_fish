<%@ page import="java.util.List" %>
<%@ page import="tsw.project.cyberfish.model.CartEntry" %>
<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Cyberfish - CARRELLO</title>
    <link rel="stylesheet" type="text/css" href="css/cart.css">
    <%@include file="WEB-INF/includes/commonHead.html"%>
</head>
<body>
    <%@include file="WEB-INF/includes/header.jsp"%>
    <div class="shoppingCart">
        <div class="title">
            Carrello
        </div>
        <div id="items">
        <%
            List<CartEntry> cart = (List<CartEntry>) session.getAttribute("carrello");
            double cartPrice = 0;
            int itemsAboveStock = 0;
            if(cart == null || cart.isEmpty()) {
        %>
                <h1> Il carrello è vuoto :( </h1>
        <%
            } else {
                for(CartEntry entry: cart) {
                    String show;
                    if(entry.getProdotto().getQuantitaRimasta() < entry.getEntry().getQuantita()) {
                        show = "";
                        itemsAboveStock ++;
                    }
                    else {
                        show = "style=\"display: none\"";
                    }
        %>
                    <div class="item" id="<%=entry.getEntry().getProdotto()%>">
                        <div class="description">
                            <%
                                if(entry.getProdotto().getTipo() == 1) {
                            %>
                                <span>Pesce</span>
                            <%
                                } else if(entry.getProdotto().getTipo() == 2) {
                            %>
                                <span>Molluschi</span>
                            <%
                                } else if(entry.getProdotto().getTipo() == 3) {
                            %>
                                <span>Crostacei</span>
                            <% } %>
                            <span><%=entry.getProdotto().getNome()%></span>
                        </div>

                        <div class="quantity">
                            <button class="plusButton" onclick="addToCart(<%=entry.getProdotto().getId()%>)">+</button>
                            <span id="quantityOf<%=entry.getProdotto().getId()%>"><%=entry.getEntry().getQuantita()%></span>
                            <button class="minusButton" onclick="subFromCart(<%=entry.getProdotto().getId()%>)">-</button>
                            <button class="deleteButton" onclick="remFromCart(<%=entry.getProdotto().getId()%>)">x</button>
                            <span id="aboveStock<%=entry.getProdotto().getId()%>" <%=show%> class="fa-solid fa-circle-exclamation" title="La quantità scelta per questo prodotto supera quella presente nelle nostre scorte!"></span>
                        </div>
                        <%
                            double prezzo = Math.ceil(entry.getProdotto().getCosto() * 100 - (entry.getProdotto().getCosto() * entry.getProdotto().getSconto())) / 100;
                            cartPrice += entry.getEntry().getQuantita() * prezzo;
                        %>

                        <div class="totalPrice" id="totalPriceOf<%=entry.getProdotto().getId()%>"><%=String.format(Locale.US, "%.2f", prezzo*entry.getEntry().getQuantita())%>&euro;</div>
                        <div class="imageContainer">
                            <a href="ProductPage?id=<%=entry.getProdotto().getId()%>"><img src="<%=entry.getProdotto().getImmagine()%>" alt="<%=entry.getProdotto().getNome()%>" class="image"></a>
                        </div>
                    </div>
        <%
                }
            }
        %>
        </div>
        <div class="placeOrderContainer">
            <div class="cartPrice"> Totale: <span id="cartPrice"><%=String.format(Locale.US, "%.2f", cartPrice)%></span>&euro;</div>
            <button class="placeOrderButton" onclick="createOrder(<%=(boolean) session.getAttribute("isLoggedIn")%>)">Procedi all'ordine</button>
        </div>

    </div>

    <script>
        setItemsAboveStock(<%=itemsAboveStock%>);
    </script>
    <%@include file="WEB-INF/includes/footer.html"%>

</body>
</html>
