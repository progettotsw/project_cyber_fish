<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <%@include file="WEB-INF/includes/commonHead.html"%>
        <script src="scripts/cards.js"></script>
        <title>Cyberfish - PESCATO</title>
    </head>
    <body style="margin: 0">
        <%@include file="WEB-INF/includes/header.jsp"%>
        <div class="checksContainer">
            <span class="checksTitle">Filtri Ricerca: </span> <br>
            <input type="checkbox" id="pesce" onchange="productsInit()" checked>
            <label for="pesce">Pesce</label>
            <input type="checkbox" id="molluschi" onchange="productsInit()" checked>
            <label for="Molluschi">Molluschi</label>
            <input type="checkbox" id="crostacei" onchange="productsInit()" checked>
            <label for="crostacei">Crostacei</label>
        </div>
        <div class="cardContainer" id="pescato"></div>
        <script>
            function productsInit(){
                setCardType(0);
                setElementID("pescato");
                let pesce = document.getElementById("pesce").checked;
                let molluschi = document.getElementById("molluschi").checked;
                let crostacei = document.getElementById("crostacei").checked;
                if(!pesce && !molluschi && !crostacei){
                    $.ajax({
                        type: "GET",
                        url:"ProductsGetter",
                        data: {
                            type: 0,
                            top: 0
                        },
                        success: function(result){
                            createCards(result);
                        }
                    });
                }
                else{
                    if(pesce) pesce = 1;
                    else pesce = -1;

                    if(molluschi) molluschi = 2;
                    else molluschi = -1;

                    if(crostacei) crostacei = 3;
                    else crostacei = -1;

                    $.ajax({
                        type:"GET",
                        url:"ProductsGetter",
                        data: {
                            type: 1,
                            top: 0,
                            pesce: pesce,
                            molluschi: molluschi,
                            crostacei: crostacei
                        },
                        success: function(result){
                            createCards(result);
                        }
                    });
                }
            }
            productsInit();
        </script>
        <%@include file="WEB-INF/includes/footer.html"%>
    </body>
</html>