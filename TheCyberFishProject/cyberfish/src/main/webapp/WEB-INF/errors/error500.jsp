<%--
  Created by IntelliJ IDEA.
  User: Iacopo
  Date: 18/07/2023
  Time: 16:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isErrorPage="true" %>
<html>
<head>
    <title>Cyberfish - 500</title>
    <%@include file="../includes/commonHead.html"%>
    <style>
        h1{
            font-family: UnisonBold,sans-serif;
            margin: auto;
            font-size: 3rem;
            text-align: center;
            color: var(--color_used_blue);
        }
        p{
            font-family: MergeOne,sans-serif;
            margin: auto;
            font-size: 1.5rem;
            text-align: center;
            color: black;
        }
    </style>
</head>
<body>
  <%@include file="../includes/header.jsp"%>
  <h1>Errore 500!</h1>
  <p>E' stato riscontrato un errore interno del server!</p>
  <%@include file="../includes/footer.html"%>
</body>
</html>
