<%
  if(session.getAttribute("isLoggedIn") == null)
  {
    session.setAttribute("isLoggedIn", false);
  }
  boolean isLoggedIn =(boolean) session.getAttribute("isLoggedIn");
%>



<header>
  <nav>
    <div class="banner">
      <ul>
        <li id="logo" style="float:left"><span id="cyber">CYBER</span><span id="fish">FISH</span></li>
        <%
        if(isLoggedIn){ %>
          <li style="float:right"><a href="LogoutServlet" class="fa-solid fa-right-from-bracket"></a></li>
          <li style="float:right"><a href="UserPage" class="fa-solid fa-user"></a></li><%
        }
        else{ %>
          <li style="float:right"><a href="login.jsp" class="fa-solid fa-right-to-bracket"></a></li> <%
        }
        %>
        <li style="float:right"><a class="fa-solid fa-cart-shopping" href="carrello.jsp"></a></li>
        <li style="float:right"><a href="ricette.jsp">Ricette</a></li>
        <li style="float:right"><a href="pescato.jsp">Pescato</a></li>
        <li style="float:right"><a href="index.jsp">Home</a></li>
      </ul>
    </div>
  </nav>
</header>