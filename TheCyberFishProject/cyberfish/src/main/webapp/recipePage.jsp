<%@ page import="tsw.project.cyberfish.controller.RecipePage" %>
<%@ page import="tsw.project.cyberfish.model.RicettaBean" %>
<%@ page import="java.util.List" %>
<%@ page import="tsw.project.cyberfish.model.IngredienteBean" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <%@include file="WEB-INF/includes/commonHead.html"%>
        <link rel="stylesheet" type="text/css" href="css/productPage.css">
        <title>Cyberfish - RICETTA</title>
        <style>
            #productDescription{
                border: none !important;
            }

        </style>
    </head>
    <body>

        <%@include file="WEB-INF/includes/header.jsp"%>

        <%
            RicettaBean ricetta = (RicettaBean) request.getAttribute("ricetta");
            String ingredienti = (String) request.getAttribute("ingredienti");
            String recipeName = ricetta.getNome();
            String recipeImage = ricetta.getImmagine();
            String recipeDescription = ricetta.getDescrizione();
        %>
        <main>
            <div class="container">
                <div class="imageContainer">
                    <img src="<%=recipeImage%>" alt="<%=recipeName%>">
                </div>
                <div class="descriptionContainer">
                    <div class="description">
                        <div class="productTag">Ricetta</div>
                        <h1 class="productName"><%=recipeName%></h1>
                        <p id="productDescription"></p>
                    </div>
                </div>
            </div>


        </main>



        <div class="separator">Prodotti nella Ricetta:</div>
        <div class="cardContainer" id="ingredients"></div>

        <script>
            setCardType(0);
            setElementID("ingredients");
            createCards(JSON.parse('<%=ingredienti%>'));
            fetch('<%=recipeDescription%>')
                .then(response => response.text())
                .then((data) =>{
                    document.getElementById("productDescription").innerText = data;
                })
        </script>

        <%@include file="WEB-INF/includes/footer.html"%>
    </body>
</html>
