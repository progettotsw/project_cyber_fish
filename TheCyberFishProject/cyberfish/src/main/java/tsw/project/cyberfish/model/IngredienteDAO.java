package tsw.project.cyberfish.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class IngredienteDAO implements DAO<IngredienteBean> {
    @Override
    public synchronized void doSave(IngredienteBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("insert into ingrediente values (?, ?)");
        statement.setInt(1, element.getProdotto());
        statement.setString(2, element.getRicetta());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized void doDelete(IngredienteBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("delete from ingrediente where prodotto = ? and ricetta = ?");
        statement.setInt(1, element.getProdotto());
        statement.setString(2, element.getRicetta());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized void doUpdate(IngredienteBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("update ingrediente set prodotto = ?, ricetta = ? where prodotto = ? and ricetta = ?");
        statement.setInt(1, element.getProdotto());
        statement.setString(2, element.getRicetta());
        statement.setInt(3, element.getProdotto());
        statement.setString(4, element.getRicetta());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized List<IngredienteBean> doRetrieveOnCondition(Condition<IngredienteBean> cond) throws SQLException {
        List<IngredienteBean> ingredienti = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from ingrediente");

        while(set.next()) {
            int prodotto = set.getInt("prodotto");
            String ricetta = set.getString("ricetta");

            IngredienteBean bean = new IngredienteBean(prodotto, ricetta);

            if(cond.test(bean))
                ingredienti.add(bean);
        }

        return ingredienti;
    }

    @Override
    public synchronized List<IngredienteBean> doRetrieveAll() throws SQLException {
        List<IngredienteBean> ingredienti = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from ingrediente");

        while(set.next()) {
            int prodotto = set.getInt("prodotto");
            String ricetta = set.getString("ricetta");

            ingredienti.add(new IngredienteBean(prodotto, ricetta));
        }

        return ingredienti;
    }
}
