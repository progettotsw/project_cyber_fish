package tsw.project.cyberfish.controller;

import com.mysql.cj.xdevapi.Client;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import tsw.project.cyberfish.model.ClienteBean;
import tsw.project.cyberfish.model.ProdottoBean;
import tsw.project.cyberfish.model.ProdottoDAO;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Objects;

@MultipartConfig
@WebServlet(name = "ProductEdit", value = "/ProductEdit")
public class ProductEdit extends HttpServlet {
    private static final String CARTELLA_UPLOAD_IMMAGINI = "images";
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        ProdottoDAO prodottoDAO = new ProdottoDAO();
        ProdottoBean bean = null;
        HttpSession session = request.getSession();
        ClienteBean user = (ClienteBean) session.getAttribute("cliente");
        if(user != null && user.isAdmin()){
            try {
                bean = prodottoDAO.doRetrieveById(id);
                prodottoDAO.doDelete(bean);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        else{
            response.sendError(500);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        ClienteBean user = (ClienteBean) session.getAttribute("cliente");
        if(user==null || !user.isAdmin()){
            response.sendError(500);
        }
        ProdottoBean bean;
        ProdottoDAO updater = new ProdottoDAO();
        int id;
        String pathOriginale = null;
        Boolean edit = Boolean.parseBoolean(request.getParameter("edit"));
        if(edit){
            id = Integer.parseInt(request.getParameter("id"));
            pathOriginale = (String) request.getParameter("pathOriginale");
            try {
                bean = updater.doRetrieveById(id);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }else{
            bean = new ProdottoBean();
        }



        String nome = (String) request.getParameter("nome");
        String descrizione = (String) request.getParameter("descrizione");
        String costo = (String) request.getParameter("costo");
        String sconto = (String) request.getParameter("sconto");
        String quantita = (String) request.getParameter("quantita");
        String tipo = (String) request.getParameter("tipo");
        String numeroOrdini =(String) request.getParameter("numeroOrdini");

        boolean errorFlag = false;

        //retrieve del bean con ID dal database



        //check correttezza input
        if(!nome.isEmpty()){
            if (nome.length() > 32 ) {
                errorFlag = true;
            }
            else{
                bean.setNome(nome);
            }
        }
        if (!costo.isEmpty()) {
            double parsedCosto = Double.parseDouble(costo);
            if(parsedCosto < 0) {
                errorFlag= true;
            }else{
                bean.setCosto(parsedCosto);
            }
        }

        if (!sconto.isEmpty()) {
            int parsedSconto = Integer.parseInt(sconto);
            if(parsedSconto < 0 || parsedSconto > 100){
                errorFlag = true;
            }else{
                bean.setSconto(parsedSconto);
            }
        }
        if(!quantita.isEmpty()){
            int parsedQuantita = Integer.parseInt(quantita);
            if(parsedQuantita < 0){
                errorFlag = true;
            }else{
                bean.setQuantitaRimasta(parsedQuantita);
            }
        }
        if(!tipo.isEmpty()){
            int parsedTipo = Integer.parseInt(tipo);
            bean.setTipo(parsedTipo);
        }

        if(!descrizione.isEmpty()){
            bean.setDescrizione(descrizione);
        }

        if (errorFlag) {
            request.setAttribute("errorflag", true);
            response.sendError(501);
        }

        //upload immagine, se non selezionata usa la precedente.
        Part filePart = request.getPart("immagine");
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        String destinazione = null;
        if (!fileName.isEmpty()) {
            String subdirectory;
            switch (Objects.requireNonNull(tipo)) {
                case "1": {
                    subdirectory = "pesce";
                    break;
                }
                case "2": {
                    subdirectory = "molluschi";
                    break;
                }
                case "3": {
                    subdirectory = "crostacei";
                    break;
                }
                default: {
                    subdirectory = "";
                    break;
                }
            }
            destinazione = CARTELLA_UPLOAD_IMMAGINI + "/" + subdirectory + "/" + fileName;
            Path pathDestinazione = Paths.get(getServletContext().getRealPath(destinazione));
            for (int i = 2; Files.exists(pathDestinazione); i++) {
                destinazione = CARTELLA_UPLOAD_IMMAGINI + "/" + subdirectory + "/" + i + "_" + fileName;
                pathDestinazione = Paths.get(getServletContext().getRealPath(destinazione));
            }

            InputStream fileInputStream = filePart.getInputStream();
            // crea CARTELLA_UPLOAD, se non esiste
            Files.createDirectories(pathDestinazione.getParent());
            // scrive il file
            Files.copy(fileInputStream, pathDestinazione);
            bean.setImmagine(destinazione);
        }
        else{
            destinazione = pathOriginale;
        }

        //update or create element inside database
        if(edit){
            try {
                updater.doUpdate(bean);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }else{
            try {
                updater.doSave(bean);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("UserPage");
        dispatcher.forward(request,response);
    }
}
