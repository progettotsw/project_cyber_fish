package tsw.project.cyberfish.model;

//functional interface for DAO method "doRetrieveOnCondition()"
public interface Condition<T> {
    boolean test(T item); //evaluates a condition on the given item
}
