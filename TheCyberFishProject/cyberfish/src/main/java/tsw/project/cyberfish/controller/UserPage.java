package tsw.project.cyberfish.controller;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import tsw.project.cyberfish.model.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "UserPage", value = "/UserPage")
public class UserPage extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        ClienteBean user = (ClienteBean) session.getAttribute("cliente");

        OrdineDAO orderRetriever = new OrdineDAO();
        ElementoOrdineDAO orderEntryRetriever = new ElementoOrdineDAO();
        ProdottoDAO productRetriever = new ProdottoDAO();

        try {
            List<OrdineBean> orders = orderRetriever.doRetrieveOnCondition(order -> order.getCliente() == user.getId());
            List<ElementoOrdineBean> entries = new ArrayList<>();
            List<ProdottoBean> products = productRetriever.doRetrieveAll();
            if(orders != null && !orders.isEmpty()) {
                for(OrdineBean order: orders) {
                    List<ElementoOrdineBean> buf = orderEntryRetriever.doRetrieveOnCondition(entry -> entry.getOrdine() == order.getId());
                    if(buf == null || buf.isEmpty())
                        throw new RuntimeException("could not find any entries for order with id = " + order.getId());
                    entries.addAll(buf);
                }
                request.setAttribute("noOrders", false);
                request.setAttribute("orders", orders);
                request.setAttribute("entries", entries);
                request.setAttribute("products", products);

                RequestDispatcher dispatcher = request.getRequestDispatcher("user.jsp");
                dispatcher.forward(request, response);
            }
            else {
                request.setAttribute("noOrders", true);

                RequestDispatcher dispatcher = request.getRequestDispatcher("user.jsp");
                dispatcher.forward(request, response);
            }
        } catch (SQLException e) {
            throw new RuntimeException("something went wrong retrieving orders");
        }


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            doGet(request,response);
    }
}
