package tsw.project.cyberfish.controller;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import tsw.project.cyberfish.model.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "RecipePage", value = "/RecipePage")
public class RecipePage extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");

        RicettaDAO recipeRetriever = new RicettaDAO();
        IngredienteDAO ingredientRetriever = new IngredienteDAO();
        ProdottoDAO productRetriever = new ProdottoDAO();

        try {
            List<RicettaBean> ricettaBuf = recipeRetriever.doRetrieveOnCondition(recipe -> recipe.getNome().equals(name));
            if(!ricettaBuf.isEmpty()) {
                RicettaBean ricetta = ricettaBuf.get(0);
                List<IngredienteBean> ingredienti = ingredientRetriever.doRetrieveOnCondition(ingrediente -> ingrediente.getRicetta().equals(ricetta.getNome()));
                String prodottiJson = "[";

                for(int i = 0; i < ingredienti.size(); i++) {
                    if(i != 0)
                        prodottiJson += ",";
                    prodottiJson += productRetriever.doRetrieveById(ingredienti.get(i).getProdotto()).stringify();
                }
                prodottiJson += "]";

                request.setAttribute("ricetta", ricetta);
                request.setAttribute("ingredienti", prodottiJson);

                RequestDispatcher dispatcher = request.getRequestDispatcher("recipePage.jsp");
                dispatcher.forward(request, response);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
