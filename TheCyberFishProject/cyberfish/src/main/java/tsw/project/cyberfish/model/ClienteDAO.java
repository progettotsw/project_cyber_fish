package tsw.project.cyberfish.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ClienteDAO implements DAO<ClienteBean> {

    public ClienteDAO() {}

    @Override
    public synchronized void doSave(ClienteBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();
        if (element.getId() == -1) {
            PreparedStatement statement = conn.prepareStatement("insert into utente (email,password,nome,cognome,admin) values (?, ?, ?, ?, ?)");
            statement.setString(1, element.getEmail());
            statement.setString(2, element.getPassword());
            statement.setString(3, element.getNome());
            statement.setString(4, element.getCognome());
            statement.setBoolean(5,element.isAdmin());
            statement.executeUpdate();

            statement.close();
        }
        else{
            PreparedStatement statement = conn.prepareStatement("insert into utente values (?, ?, ?, ?, ?, ?)");
            statement.setInt(1, element.getId());
            statement.setString(2, element.getEmail());
            statement.setString(3, element.getPassword());
            statement.setString(4, element.getNome());
            statement.setString(5, element.getCognome());
            statement.setBoolean(6,element.isAdmin());
            statement.executeUpdate();

            statement.close();
        }

        conn.close();
    }

    @Override
    public synchronized void doDelete(ClienteBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("delete from utente where id = ?");
        statement.setInt(1, element.getId());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized void doUpdate(ClienteBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("update utente set id= ?, email = ?, password = ?, nome = ?, cognome = ?, admin = ? where id= ?");
        statement.setInt(1 , element.getId());
        statement.setString(2,element.getEmail());
        statement.setString(3, element.getPassword());
        statement.setString(4, element.getNome());
        statement.setString(5, element.getCognome());
        statement.setBoolean(6,element.isAdmin());
        statement.setInt(7, element.getId());
        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized List<ClienteBean> doRetrieveOnCondition(Condition<ClienteBean> cond) throws SQLException {
        List<ClienteBean> clienti = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from utente");

        while(set.next()) {
            int id = set.getInt("id");
            String email = set.getString("email");
            String password = set.getString("password");
            String nome = set.getString("nome");
            String cognome = set.getString("cognome");
            boolean admin = set.getBoolean("admin");
            ClienteBean cliente = new ClienteBean(id,email, password, nome, cognome,admin);

            if(cond.test(cliente))
                clienti.add(cliente);
        }

        statement.close();
        conn.close();
        return clienti;
    }

    @Override
    public synchronized List<ClienteBean> doRetrieveAll() throws SQLException {
        List<ClienteBean> clienti = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from utente");

        while(set.next()) {
            int id = set.getInt("id");
            String email = set.getString("email");
            String password = set.getString("password");
            String nome = set.getString("nome");
            String cognome = set.getString("cognome");
            boolean admin = set.getBoolean("admin");
            clienti.add(new ClienteBean(id,email, password, nome, cognome,admin));
        }

        statement.close();
        conn.close();
        return clienti;
    }
}
