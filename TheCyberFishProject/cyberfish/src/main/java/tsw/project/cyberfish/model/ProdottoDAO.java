package tsw.project.cyberfish.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProdottoDAO implements DAO<ProdottoBean> {

    public ProdottoDAO(){}

    @Override
    public synchronized void doSave(ProdottoBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement(
                "insert into prodotto(tipo, nome, costo, sconto, quantita_rimasta, descrizione, immagine, numero_ordini) values " +
                "(?, ?, ?, ?, ?, ?, ?, ?)");

        statement.setInt(1, element.getTipo());
        statement.setString(2, element.getNome());
        statement.setDouble(3, element.getCosto());
        statement.setInt(4, element.getSconto());
        statement.setInt(5, element.getQuantitaRimasta());
        statement.setString(6, element.getDescrizione());
        statement.setString(7, element.getImmagine());
        statement.setInt(8, element.getNumeroOrdini());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized void doDelete(ProdottoBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("delete from prodotto where id = ?");
        statement.setInt(1, element.getId());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized void doUpdate(ProdottoBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement(
                "update prodotto " +
                "set id = ?, tipo = ?, nome = ?, costo = ?, sconto = ?, quantita_rimasta = ?, descrizione = ?, immagine = ?, numero_ordini = ? " +
                "where id = ?");
        statement.setInt(1, element.getId());
        statement.setInt(2, element.getTipo());
        statement.setString(3, element.getNome());
        statement.setDouble(4, element.getCosto());
        statement.setInt(5, element.getSconto());
        statement.setInt(6, element.getQuantitaRimasta());
        statement.setString(7, element.getDescrizione());
        statement.setString(8, element.getImmagine());
        statement.setInt(9, element.getNumeroOrdini());
        statement.setInt(10, element.getId());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    public synchronized ProdottoBean doRetrieveById(int id) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("select * from prodotto where id = ?");
        statement.setInt(1, id);

        ResultSet set = statement.executeQuery();

        while (set.next()) {
            int tipo = set.getInt("tipo");
            String nome = set.getString("nome");
            double costo = set.getDouble("costo");
            int sconto = set.getInt("sconto");
            int quantitaRimasta = set.getInt("quantita_rimasta");
            String descrizione = set.getString("descrizione");
            String immagine = set.getString("immagine");
            int numeroOrdini = set.getInt("numero_ordini");
            return new ProdottoBean(id, tipo, nome, costo, sconto, quantitaRimasta, descrizione, immagine, numeroOrdini);
        }

        throw new SQLException("no products with id = " + id);
    }

    @Override
    public synchronized List<ProdottoBean> doRetrieveOnCondition(Condition<ProdottoBean> cond) throws SQLException {
        List<ProdottoBean> prodotti = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from prodotto");

        while(set.next()) {
            int id = set.getInt("id");
            int tipo = set.getInt("tipo");
            String nome = set.getString("nome");
            double costo = set.getDouble("costo");
            int sconto = set.getInt("sconto");
            int quantitaRimasta = set.getInt("quantita_rimasta");
            String descrizione = set.getString("descrizione");
            String immagine = set.getString("immagine");
            int numeroOrdini = set.getInt("numero_ordini");
            ProdottoBean prodotto = new ProdottoBean(id, tipo, nome, costo, sconto, quantitaRimasta, descrizione, immagine, numeroOrdini);

            if(cond.test(prodotto))
                prodotti.add(prodotto);
        }

        statement.close();
        conn.close();
        return prodotti;
    }

    @Override
    public synchronized List<ProdottoBean> doRetrieveAll() throws SQLException {
        List<ProdottoBean> prodotti = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from prodotto");

        while(set.next()) {
            int id = set.getInt("id");
            int tipo = set.getInt("tipo");
            String nome = set.getString("nome");
            double costo = set.getDouble("costo");
            int sconto = set.getInt("sconto");
            int quantitaRimasta = set.getInt("quantita_rimasta");
            String descrizione = set.getString("descrizione");
            String immagine = set.getString("immagine");
            int numeroOrdini = set.getInt("numero_ordini");

            prodotti.add(new ProdottoBean(id, tipo, nome, costo, sconto, quantitaRimasta, descrizione, immagine, numeroOrdini));
        }

        statement.close();
        conn.close();
        return prodotti;
    }
}
