package tsw.project.cyberfish.model;

public class IngredienteBean {

    private int prodotto;
    private String ricetta;

    public IngredienteBean(int prodotto, String ricetta) {
        this.prodotto = prodotto;
        this.ricetta = ricetta;
    }

    public IngredienteBean() {
    }

    public int getProdotto() {
        return prodotto;
    }

    public String getRicetta() {
        return ricetta;
    }

    public void setProdotto(int prodotto) {
        this.prodotto = prodotto;
    }

    public void setRicetta(String ricetta) {
        this.ricetta = ricetta;
    }
}
