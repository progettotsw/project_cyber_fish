package tsw.project.cyberfish.controller;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import tsw.project.cyberfish.model.*;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.regex.Pattern;

@WebServlet(name = "CheckOutValidator", value = "/CheckOutValidator")
public class CheckOutValidator extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nome = request.getParameter("nome");
        String email = request.getParameter("email");
        String indirizzo = request.getParameter("indirizzo");
        String citta = request.getParameter("citta");
        String CAP = request.getParameter("CAP");
        String month = request.getParameter("meseScadenza");
        String nomeCarta = request.getParameter("nomeCarta");
        String numeroCarta = request.getParameter("numeroCarta");
        String cvv = request.getParameter("cvv");
        boolean spedizioneRapida = Boolean.parseBoolean(request.getParameter("spedizioneRapida"));

        HttpSession session = request.getSession();

        List<CartEntry> cart = (List<CartEntry>) session.getAttribute("carrello");
        ClienteBean user = (ClienteBean) session.getAttribute("cliente");
        if(cart == null || cart.isEmpty() || user == null) {
            throw new RuntimeException("attempt to order an empty cart");
        }

        double totalPrice = 0;
        for(CartEntry entry: cart) {
            if(entry.getProdotto().getQuantitaRimasta() < entry.getEntry().getQuantita())
                throw new RuntimeException("attempt to order more of the " + entry.getProdotto().getId() + " item than the amount remaining");

            totalPrice += Math.ceil((entry.getProdotto().getCosto() * 100 - (entry.getProdotto().getCosto() * entry.getProdotto().getSconto()))) / 100 * entry.getEntry().getQuantita();
        }
        if(spedizioneRapida)
            totalPrice += 5;

        OrdineBean ordine = new OrdineBean(Date.valueOf(LocalDate.now()), totalPrice, spedizioneRapida, user.getId(), nome, email, indirizzo, citta, CAP);
        if(!validateOrder(ordine) || !validatePaymentInfo(nomeCarta, numeroCarta, cvv, month))
            response.sendError(501);

        OrdineDAO orderSaver = new OrdineDAO();
        ElementoOrdineDAO orderEntrySaver = new ElementoOrdineDAO();
        ProdottoDAO productUpdater = new ProdottoDAO();
        CarrelloDAO entryDeleter = new CarrelloDAO();

        try {
            orderSaver.doSave(ordine);
            for(CartEntry entry: cart) {
                entry.getProdotto().setQuantitaRimasta(entry.getProdotto().getQuantitaRimasta() - entry.getEntry().getQuantita());
                entry.getProdotto().setNumeroOrdini(entry.getProdotto().getNumeroOrdini() + entry.getEntry().getQuantita());
                productUpdater.doUpdate(entry.getProdotto());
                ElementoOrdineBean orderEntry = new ElementoOrdineBean();
                orderEntry.setOrdine(orderSaver.getLastInsertID());
                orderEntry.setProdotto(entry.getProdotto().getId());
                orderEntry.setPrezzoCorrente(entry.getProdotto().getCosto() - (entry.getProdotto().getCosto() * entry.getProdotto().getSconto() / 100));
                orderEntry.setQuantitaAcquistata(entry.getEntry().getQuantita());
                orderEntrySaver.doSave(orderEntry);
                entryDeleter.doDelete(entry.getEntry());
            }
            session.setAttribute("carrello", null);

            RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
            dispatcher.forward(request, response);

        } catch (SQLException e) {
            throw new RuntimeException("error saving order");
        }
    }

    private boolean validateOrder(OrdineBean ordine) {
        return ordine.getIndirizzo().length() <= 255
                && ordine.getNomeIndirizzo().length() <= 255
                && ordine.getEmailIndirizzo().length() <= 255
                && ordine.getCittaIndirizzo().length() <= 50
                && Pattern.compile("^[0-9]{5}$").matcher(ordine.getCAPIndirizzo()).matches();
    }

    private boolean validatePaymentInfo(String cardName, String cardNumber, String cvv, String meseScadenza) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        java.util.Date date;
        try {
            date = dateFormat.parse(meseScadenza);
        } catch (ParseException e) {
            return false;
        }
        boolean isExpired = date.before(new java.util.Date());

        return cardName != null && !cardName.isEmpty() && !isExpired
                && cardNumber != null && (cardNumber.length() == 19)
                && Pattern.compile("^(([0-9]{4}[- ]){3}[0-9]{4})$").matcher(cardNumber).matches()
                && cvv != null && cvv.length() == 3
                && Pattern.compile("^[0-9]{3}$").matcher(cvv).matches();
    }
}
