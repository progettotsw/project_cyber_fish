package tsw.project.cyberfish.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ElementoOrdineDAO implements DAO<ElementoOrdineBean> {
    public ElementoOrdineDAO() {}

    @Override
    public synchronized void doSave(ElementoOrdineBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("insert into elemento_ordine values(?, ?, ?, ?)");
        statement.setInt(1, element.getProdotto());
        statement.setInt(2, element.getOrdine());
        statement.setDouble(3, element.getPrezzoCorrente());
        statement.setInt(4, element.getQuantitaAcquistata());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized void doDelete(ElementoOrdineBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("delete from elemento_ordine where ordine = ? and prodotto = ?");
        statement.setInt(1, element.getOrdine());
        statement.setInt(1, element.getProdotto());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized void doUpdate(ElementoOrdineBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("update elemento_ordine set prodotto = ?, ordine = ?, prezzo_corrente = ?, quantita_acquistata = ? where prodotto = ? and ordine = ?");
        statement.setInt(1, element.getProdotto());
        statement.setInt(2, element.getOrdine());
        statement.setDouble(3, element.getPrezzoCorrente());
        statement.setInt(4, element.getQuantitaAcquistata());
        statement.setInt(5, element.getProdotto());
        statement.setInt(6, element.getOrdine());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized List<ElementoOrdineBean> doRetrieveOnCondition(Condition<ElementoOrdineBean> cond) throws SQLException {
        List<ElementoOrdineBean> elementi = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from elemento_ordine");
        while(set.next()) {
            int prodotto = set.getInt("prodotto");
            int ordine = set.getInt("ordine");
            double prezzoCorrente = set.getDouble("prezzo_corrente");
            int quantitaAcquistata = set.getInt("quantita_acquistata");
            ElementoOrdineBean elemento = new ElementoOrdineBean(prodotto, ordine, prezzoCorrente, quantitaAcquistata);

            if(cond.test(elemento))
                elementi.add(elemento);
        }

        statement.close();
        conn.close();
        return elementi;
    }

    @Override
    public synchronized List<ElementoOrdineBean> doRetrieveAll() throws SQLException {
        List<ElementoOrdineBean> elementi = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from elemento_ordine");
        while(set.next()) {
            int prodotto = set.getInt("prodotto");
            int ordine = set.getInt("ordine");
            double prezzoCorrente = set.getDouble("prezzo_corrente");
            int quantitaAcquistata = set.getInt("quantita_acquistata");
            elementi.add(new ElementoOrdineBean(prodotto, ordine, prezzoCorrente, quantitaAcquistata));
        }

        statement.close();
        conn.close();
        return elementi;
    }
}
