package tsw.project.cyberfish.model;

public class ElementoOrdineBean {
    private int prodotto;
    private int ordine;
    private double prezzoCorrente;

    private int quantitaAcquistata;

    public ElementoOrdineBean(){}

    public ElementoOrdineBean(int prodotto, int ordine, double prezzoCorrente, int quantitaAcquistata) {
        this.prodotto = prodotto;
        this.ordine = ordine;
        this.prezzoCorrente = prezzoCorrente;
        this.quantitaAcquistata = quantitaAcquistata;
    }

    public int getProdotto() {
        return prodotto;
    }

    public void setProdotto(int prodotto) {
        this.prodotto = prodotto;
    }

    public int getOrdine() {
        return ordine;
    }

    public void setOrdine(int ordine) {
        this.ordine = ordine;
    }

    public double getPrezzoCorrente() {
        return prezzoCorrente;
    }

    public void setPrezzoCorrente(double prezzoCorrente) {
        this.prezzoCorrente = prezzoCorrente;
    }

    public int getQuantitaAcquistata() {
        return quantitaAcquistata;
    }

    public void setQuantitaAcquistata(int quantitaAcquistata) {
        this.quantitaAcquistata = quantitaAcquistata;
    }
}
