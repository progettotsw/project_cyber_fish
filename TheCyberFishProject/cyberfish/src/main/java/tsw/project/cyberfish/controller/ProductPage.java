package tsw.project.cyberfish.controller;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import tsw.project.cyberfish.model.ProdottoBean;
import tsw.project.cyberfish.model.ProdottoDAO;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "ProductPage", value = "/ProductPage")
public class ProductPage extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        boolean edit = Boolean.parseBoolean(request.getParameter("edit"));
        ProdottoDAO retriever = new ProdottoDAO();
        List<ProdottoBean> productList;
        String json;
        try {
            productList = retriever.doRetrieveOnCondition(prod -> prod.getId() == id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        request.setAttribute("prodottoBean", productList.get(0));
        RequestDispatcher dispatcher;
        if(edit){
            dispatcher = request.getRequestDispatcher("productPageEdit.jsp");
        }
        else{

            dispatcher = request.getRequestDispatcher("productPage.jsp");

        }
        dispatcher.forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
