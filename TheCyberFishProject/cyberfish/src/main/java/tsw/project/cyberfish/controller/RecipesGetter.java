package tsw.project.cyberfish.controller;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import tsw.project.cyberfish.model.RicettaBean;
import tsw.project.cyberfish.model.RicettaDAO;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;




@WebServlet(name="RecipesGetter",value="/RecipesGetter")
public class RecipesGetter extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RicettaDAO retriever = new RicettaDAO();
        List<RicettaBean> recipes;
        try {
            recipes = retriever.doRetrieveAll();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        String res = "[";
        for (int i = 0; i < recipes.size(); i++) {
            if (i == 0) {
                res += recipes.get(i).stringify();
            } else {
                res += ", " + recipes.get(i).stringify();
            }
        }
        res += "]";
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        out.println(res);
    }
}
