package tsw.project.cyberfish.controller;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import tsw.project.cyberfish.model.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@WebServlet(name = "AuthServlet", value = "/AuthServlet")
public class AuthServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String type = request.getParameter("type");
        boolean outcome = type.equals("login") ? login(request) : register(request);

        if(outcome) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
            HttpSession session = request.getSession();
            ClienteBean user = (ClienteBean) session.getAttribute("cliente");
            List<CartEntry> cart = (List<CartEntry>) session.getAttribute("carrello");
            List<CarrelloBean> entries;
            CarrelloDAO entryRetriever = new CarrelloDAO();
            ProdottoDAO productRetriever = new ProdottoDAO();
            if(cart != null && !cart.isEmpty()) {
                for(CartEntry entry: cart)
                    entry.getEntry().setCliente(user.getId());

                try {
                    entries = entryRetriever.doRetrieveOnCondition(entry -> entry.getCliente() == user.getId());
                    boolean found;
                    for(CarrelloBean entry: entries) {
                        found = false;
                        for(CartEntry cartEntry: cart) {
                            if(cartEntry.getEntry().getProdotto() == entry.getProdotto()) {
                                cartEntry.getEntry().setQuantita(cartEntry.getEntry().getQuantita() + entry.getQuantita());
                                found = true;
                                break;
                            }
                        }
                        if(found) continue;
                        cart.add(new CartEntry(entry, productRetriever.doRetrieveById(entry.getProdotto())));
                    }
                    CartSaver.doSave(cart);
                } catch (SQLException ignored) {
                }
            }else{
                if(cart == null){
                    cart = new ArrayList<>();
                }
                try {
                    entries = entryRetriever.doRetrieveOnCondition(entry -> entry.getCliente() == user.getId());
                    for(CarrelloBean bean: entries){
                        cart.add(new CartEntry(bean,productRetriever.doRetrieveById(bean.getProdotto())));
                    }
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }

            }
            session.setAttribute("carrello", cart);

            dispatcher.forward(request, response);
        }
        else {
            String resource = type.equals("login") ? "login.jsp" : "registrazione.jsp";
            request.setAttribute("outcome", "false");
            RequestDispatcher dispatcher = request.getRequestDispatcher(resource);
            dispatcher.forward(request, response);
        }
    }

    private boolean login(HttpServletRequest request) {
        ClienteDAO retriever = new ClienteDAO();
        final String email = request.getParameter("email");
        final String password = request.getParameter("password");
        ClienteBean user = new ClienteBean();
        user.setEmail(email);
        user.setPassword(password);
        List<ClienteBean> c;
        try {
            c = retriever.doRetrieveOnCondition(cliente -> cliente.getEmail().equals(user.getEmail()) && cliente.getPassword().equals(user.getPassword()));
        } catch (SQLException e) {
            return false;
        }

        if(c.size() != 1)
            return false;
        ClienteBean cliente = c.get(0);
        String name = cliente.getNome();

        HttpSession session = request.getSession();
        session.setAttribute("isLoggedIn", true);
        session.setAttribute("cliente", cliente);


        return true;
    }

    private boolean register(HttpServletRequest request) {
        final String email = request.getParameter("email");
        final String password = request.getParameter("password");
        final String nome = request.getParameter("name");
        final String cognome = request.getParameter("surname");
        ClienteBean cliente = new ClienteBean(-1,email, password, nome, cognome,false);
        if(!validateUserData(cliente)) {
            return false;
        }
        cliente.setPassword(password);
        ClienteDAO saver = new ClienteDAO();
        List<ClienteBean> c;
        try {
            saver.doSave(cliente);
            c = saver.doRetrieveOnCondition(user -> user.getEmail().equals(cliente.getEmail()));
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return false;
        }
        if(c.size() == 0){
            throw new RuntimeException();
        }

        HttpSession session = request.getSession();
        session.setAttribute("isLoggedIn", true);
        session.setAttribute("cliente", c.get(0));
        return true;
    }

    private boolean validateUserData(ClienteBean user) {
        String email = user.getEmail();
        String password = user.getPassword();
        boolean emailMatches = Pattern.compile("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$").matcher(email).matches();
        boolean passwordMatches = Pattern.compile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+").matcher(password).matches();

        return !email.isEmpty() && emailMatches && email.length() <= 255
                && passwordMatches && password.length() >= 8 && password.length() <= 32
                && user.getNome().length() <= 24
                && user.getCognome().length() <= 24;
    }
}
