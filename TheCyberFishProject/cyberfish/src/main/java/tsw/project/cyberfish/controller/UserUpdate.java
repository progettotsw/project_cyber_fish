package tsw.project.cyberfish.controller;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import tsw.project.cyberfish.model.ClienteBean;
import tsw.project.cyberfish.model.ClienteDAO;

import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "UserUpdate", value = "/UserUpdate")
public class UserUpdate extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int type = Integer.parseInt(request.getParameter("type"));
        String value = request.getParameter("value");
        ClienteDAO updater = new ClienteDAO();
        HttpSession session = request.getSession();
        ClienteBean user = (ClienteBean) session.getAttribute("cliente");
        switch (type)
        {
            case 1:
            {
                user.setNome(value);
                try {
                    updater.doUpdate(user);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
                break;
            }
            case 2:
            {
                user.setCognome(value);
                try {
                    updater.doUpdate(user);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
                break;
            }
            case 3:
            {
                user.setEmail(value);
                try{
                    updater.doUpdate(user);
                }catch (SQLException e){
                    throw new RuntimeException(e);
                }
                break;
            }
            case 4:
            {
                user.setPassword(value);
                try {
                    updater.doUpdate(user);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
                break;
            }
            default:
            {
                throw new RuntimeException("type is invalid!");
            }
        }
    }
}
