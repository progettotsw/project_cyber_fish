package tsw.project.cyberfish.model;

public class CarrelloBean {
    private int cliente;
    private int prodotto;
    private int quantita;

    public CarrelloBean(){}

    public CarrelloBean(int cliente, int prodotto, int quantita) {
        this.cliente = cliente;
        this.prodotto = prodotto;
        this.quantita = quantita;
    }

    public int getCliente() {
        return cliente;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public int getProdotto() {
        return prodotto;
    }

    public void setProdotto(int prodotto) {
        this.prodotto = prodotto;
    }

    public int getQuantita() {
        return quantita;
    }

    public void setQuantita(int quantita) {
        this.quantita = quantita;
    }
}
