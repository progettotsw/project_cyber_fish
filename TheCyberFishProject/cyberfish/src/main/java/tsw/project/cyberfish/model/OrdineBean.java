package tsw.project.cyberfish.model;

import java.sql.Date;

public class OrdineBean {
    private int id;
    private Date dataAcquisto;
    private double prezzoTotale;
    private boolean spedizioneRapida;
    private int cliente;

    private String nomeIndirizzo;
    private String emailIndirizzo;
    private String indirizzo;
    private String cittaIndirizzo;
    private String CAPIndirizzo;

    public OrdineBean(){}

    public OrdineBean( Date dataAcquisto, double prezzoTotale, boolean spedizioneRapida, int cliente, String nomeIndirizzo, String emailIndirizzo, String indirizzo, String cittaIndirizzo, String CAPIndirizzo) {
        this.dataAcquisto = dataAcquisto;
        this.prezzoTotale = prezzoTotale;
        this.spedizioneRapida = spedizioneRapida;
        this.cliente = cliente;
        this.nomeIndirizzo = nomeIndirizzo;
        this.emailIndirizzo = emailIndirizzo;
        this.indirizzo = indirizzo;
        this.cittaIndirizzo = cittaIndirizzo;
        this.CAPIndirizzo = CAPIndirizzo;
    }

    public OrdineBean(int id, Date dataAcquisto, double prezzoTotale, boolean spedizioneRapida, int cliente, String nomeIndirizzo, String emailIndirizzo, String indirizzo, String cittaIndirizzo, String CAPIndirizzo) {
        this.id = id;
        this.dataAcquisto = dataAcquisto;
        this.prezzoTotale = prezzoTotale;
        this.spedizioneRapida = spedizioneRapida;
        this.cliente = cliente;
        this.nomeIndirizzo = nomeIndirizzo;
        this.emailIndirizzo = emailIndirizzo;
        this.indirizzo = indirizzo;
        this.cittaIndirizzo = cittaIndirizzo;
        this.CAPIndirizzo = CAPIndirizzo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDataAcquisto() {
        return dataAcquisto;
    }

    public void setDataAcquisto(Date dataAcquisto) {
        this.dataAcquisto = dataAcquisto;
    }

    public double getPrezzoTotale() {
        return prezzoTotale;
    }

    public void setPrezzoTotale(double prezzoTotale) {
        this.prezzoTotale = prezzoTotale;
    }

    public boolean isSpedizioneRapida() {
        return spedizioneRapida;
    }

    public void setSpedizioneRapida(boolean spedizioneRapida) {
        this.spedizioneRapida = spedizioneRapida;
    }

    public int getCliente() {
        return cliente;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public String getNomeIndirizzo() {
        return nomeIndirizzo;
    }

    public void setNomeIndirizzo(String nomeIndirizzo) {
        this.nomeIndirizzo = nomeIndirizzo;
    }

    public String getEmailIndirizzo() {
        return emailIndirizzo;
    }

    public void setEmailIndirizzo(String emailIndirizzo) {
        this.emailIndirizzo = emailIndirizzo;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getCittaIndirizzo() {
        return cittaIndirizzo;
    }

    public void setCittaIndirizzo(String cittaIndirizzo) {
        this.cittaIndirizzo = cittaIndirizzo;
    }

    public String getCAPIndirizzo() {
        return CAPIndirizzo;
    }

    public void setCAPIndirizzo(String CAPIndirizzo) {
        this.CAPIndirizzo = CAPIndirizzo;
    }
}
