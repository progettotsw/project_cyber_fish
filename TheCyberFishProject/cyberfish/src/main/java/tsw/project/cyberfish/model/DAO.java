package tsw.project.cyberfish.model;

import java.sql.SQLException;
import java.util.List;

public interface DAO<T> {
     void  doSave(T element) throws SQLException; //supposed to save an element into the database, if present throws SQLException
     void  doDelete(T element) throws SQLException; //supposed to delete an element from the database
     void  doUpdate(T element) throws SQLException; //supposed to update an element into the database
     List<T>  doRetrieveOnCondition(Condition<T> cond) throws SQLException; //returns a list of elements that satisfy the condition given (Can even use lambdas with it OMG)
     List<T>  doRetrieveAll() throws SQLException; //returns a list of all the elements
}
