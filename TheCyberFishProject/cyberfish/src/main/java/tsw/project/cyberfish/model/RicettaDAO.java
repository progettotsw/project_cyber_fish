package tsw.project.cyberfish.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RicettaDAO implements DAO<RicettaBean> {
    public RicettaDAO(){}

    @Override
    public synchronized void doSave(RicettaBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("insert into ricetta values (?, ?, ?)");
        statement.setString(1, element.getNome());
        statement.setString(2, element.getDescrizione());
        statement.setString(3, element.getImmagine());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized void doDelete(RicettaBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("delete from ricetta where nome = ?");
        statement.setString(1, element.getNome());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized void doUpdate(RicettaBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("update ricetta set nome = ?, descrizione = ?, immagine = ? where nome = ?");
        statement.setString(1, element.getNome());
        statement.setString(2, element.getDescrizione());
        statement.setString(3, element.getImmagine());
        statement.setString(4, element.getNome());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized List<RicettaBean> doRetrieveOnCondition(Condition<RicettaBean> cond) throws SQLException {
        List<RicettaBean> ricette = new ArrayList<>();

        Connection conn = DBConnector.getConnection();
        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from ricetta");

        while(set.next()) {
            String nome = set.getString("nome");
            String descrizione = set.getString("descrizione");
            String  immagine = set.getString("immagine");
            RicettaBean ricetta = new RicettaBean(nome, descrizione, immagine);

            if(cond.test(ricetta))
                ricette.add(ricetta);
        }

        statement.close();
        conn.close();
        return ricette;
    }

    @Override
    public synchronized List<RicettaBean> doRetrieveAll() throws SQLException {
        List<RicettaBean> ricette = new ArrayList<>();

        Connection conn = DBConnector.getConnection();
        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from ricetta");

        while(set.next()) {
            String nome = set.getString("nome");
            String descrizione = set.getString("descrizione");
            String  immagine = set.getString("immagine");

            ricette.add( new RicettaBean(nome, descrizione, immagine));
        }

        statement.close();
        conn.close();
        return ricette;
    }
}
