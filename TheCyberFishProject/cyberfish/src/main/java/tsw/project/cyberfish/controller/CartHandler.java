package tsw.project.cyberfish.controller;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import tsw.project.cyberfish.model.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "CartHandler", value = "/CartHandler")
public class CartHandler extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String mode = request.getParameter("mode");
        HttpSession session = request.getSession();
        List<CartEntry> cart = (List<CartEntry>) session.getAttribute("carrello");
        int itemId = Integer.parseInt(request.getParameter("id"));
        ClienteBean user = (ClienteBean) session.getAttribute("cliente");
        boolean isLoggedIn = (boolean) session.getAttribute("isLoggedIn");
        CarrelloDAO saver = new CarrelloDAO();
        int newQuantity = 0;
        int stockQuantity = 0;
        boolean remove = false;
        double itemPrice = 0;

        switch (mode) {
            case "add": {
                boolean found = false;


                if (cart == null) {
                    cart = new ArrayList<>();
                }

                for(CartEntry entry: cart) {
                    if(entry.getEntry().getProdotto() == itemId) {
                        entry.getEntry().setQuantita(entry.getEntry().getQuantita() + 1);

                        newQuantity = entry.getEntry().getQuantita();
                        stockQuantity = entry.getProdotto().getQuantitaRimasta();
                        itemPrice = Math.ceil(entry.getProdotto().getCosto() * 100 - (entry.getProdotto().getCosto() * entry.getProdotto().getSconto())) / 100;

                        if(isLoggedIn){
                            try {
                                List<CarrelloBean> check = saver.doRetrieveOnCondition(bean -> bean.getCliente() == entry.getEntry().getCliente() && bean.getProdotto() == entry.getEntry().getProdotto());
                                if(!check.isEmpty())
                                    saver.doUpdate(entry.getEntry());
                                else
                                    saver.doSave(entry.getEntry());
                            } catch (SQLException e) {
                                throw new RuntimeException(e);
                            }
                        }
                        found = true;
                        break;
                    }
                }
                if(!found) {
                    ProdottoDAO retriever = new ProdottoDAO();
                    try {
                        CarrelloBean entry = new CarrelloBean(user == null ? -1 : user.getId(), itemId, 1);
                        CartEntry buf = new CartEntry(
                                entry,
                                retriever.doRetrieveOnCondition(prodotto -> prodotto.getId() == itemId).get(0)
                        );

                        cart.add(buf);

                        newQuantity = 1;
                        stockQuantity = buf.getProdotto().getQuantitaRimasta();
                        itemPrice = Math.ceil(buf.getProdotto().getCosto() * 100 - (buf.getProdotto().getCosto() * buf.getProdotto().getSconto())) / 100;

                        if (isLoggedIn) {
                            try {
                                List<CarrelloBean> check = saver.doRetrieveOnCondition(bean -> bean.getCliente() == entry.getCliente() && bean.getProdotto() == entry.getProdotto());
                                if (!check.isEmpty())
                                    saver.doUpdate(entry);
                                else
                                    saver.doSave(entry);
                            } catch (SQLException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    } catch (SQLException ignored) {
                    }
                }

                break;
            }
            case "sub": {
                if (cart == null) return;

                for(int i = 0; i < cart.size(); i++) {
                    if(cart.get(i).getEntry().getProdotto() == itemId) {
                        cart.get(i).getEntry().setQuantita(cart.get(i).getEntry().getQuantita() - 1);

                        newQuantity = cart.get(i).getEntry().getQuantita();
                        stockQuantity = cart.get(i).getProdotto().getQuantitaRimasta();
                        itemPrice = Math.ceil(cart.get(i).getProdotto().getCosto() * 100 - (cart.get(i).getProdotto().getCosto() * cart.get(i).getProdotto().getSconto())) / 100;

                        if(cart.get(i).getEntry().getQuantita() <= 0) {
                            try {
                                CarrelloDAO cartDel = new CarrelloDAO();
                                cartDel.doDelete(cart.get(i).getEntry());
                            } catch (SQLException ignored) {
                            }
                            cart.remove(i);
                            session.setAttribute("carrello", cart);

                            remove = true;

                            break;
                        }
                        try {
                            saver.doUpdate(cart.get(i).getEntry());
                        } catch (SQLException ignored) {
                        }
                        break;
                    }
                }
                break;
            }
            case "remove": {
                if (cart == null) return;

                for(int i = 0; i < cart.size(); i++) {
                    if(cart.get(i).getEntry().getProdotto() == itemId) {
                        try {
                            CarrelloDAO cartDel = new CarrelloDAO();
                            cartDel.doDelete(cart.get(i).getEntry());
                        } catch (SQLException ignored) {
                        }
                        cart.remove(i);
                        session.setAttribute("carrello", cart);

                        remove = true;

                        break;
                    }
                }
                break;
            }// non utilizzato
            case "save": {
                try {
                    if(isLoggedIn){
                        CartSaver.doSave(cart);
                    }
                } catch (SQLException ignored) {
                }
                break;
            }
            default: {
                throw new RuntimeException("mode for CartHandler wrong or unset");
            }
        }

        session.setAttribute("carrello", cart);
        response.setContentType("application/json");
        response.getWriter().println("{\"remove\":" + remove + ",\"newQuantity\":" + newQuantity + ",\"stockQuantity\":" + stockQuantity + ",\"itemPrice\":" + itemPrice + "}");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
