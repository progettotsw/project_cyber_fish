package tsw.project.cyberfish.model;

public class RicettaBean {
    private String nome;
    private String descrizione;
    private String immagine;

    public RicettaBean(String nome, String descrizione, String immagine) {
        this.nome = nome;
        this.descrizione = descrizione;
        this.immagine = immagine;
    }

    public RicettaBean() {
    }

    public String stringify(){
        String json = "{";
        json += "\"nome\": \"" +   this.nome + "\", ";
        json += "\"immagine\": \"" + this.immagine + "\", ";
        json += "\"descrizione\": \"" + this.descrizione + "\"}";

        return json;
    }

    public String getNome() {
        return nome;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public String getImmagine() {
        return immagine;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public void setImmagine(String immagine) {
        this.immagine = immagine;
    }

    @Override
    public String toString() {
        return "RicettaBean{" +
                "nome='" + nome + '\'' +
                ", descrizione='" + descrizione + '\'' +
                ", immagine='" + immagine + '\'' +
                '}';
    }


}
