package tsw.project.cyberfish.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrdineDAO implements DAO<OrdineBean> {

    public OrdineDAO(){}

    public synchronized int getLastInsertID() throws SQLException {
        Connection conn = DBConnector.getConnection();
        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("SELECT LAST_INSERT_ID(id) from ordine order by LAST_INSERT_ID(id) desc limit 1;");

        if(set.next())
            return set.getInt("LAST_INSERT_ID(id)");

        return -1;
    }

    @Override
    public synchronized void doSave(OrdineBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("insert into ordine(data_acquisto, prezzo_totale, spedizione_rapida, utente, nome_indirizzo, email_indirizzo, indirizzo, citta_indirizzo, CAP_indirizzo) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        statement.setDate(1, element.getDataAcquisto());
        statement.setDouble(2, element.getPrezzoTotale());
        statement.setBoolean(3, element.isSpedizioneRapida());
        statement.setInt(4, element.getCliente());
        statement.setString(5,element.getNomeIndirizzo());
        statement.setString(6,element.getEmailIndirizzo());
        statement.setString(7,element.getIndirizzo());
        statement.setString(8,element.getCittaIndirizzo());
        statement.setString(9, element.getCAPIndirizzo());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized void doDelete(OrdineBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("delete from ordine where id = ?");
        statement.setInt(1, element.getId());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized void doUpdate(OrdineBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("update ordine set id = ?, data_acquisto = ?, prezzo_totale = ?, spedizione_rapida = ?, utente = ?, nome_indirizzo = ?, email_indirizzo =?, indirizzo=?, citta_indirizzo=?, CAP_indirizzo=? where id = ?");
        statement.setInt(1, element.getId());
        statement.setDate(2, element.getDataAcquisto());
        statement.setDouble(3, element.getPrezzoTotale());
        statement.setBoolean(4, element.isSpedizioneRapida());
        statement.setInt(5, element.getCliente());
        statement.setString(6, element.getNomeIndirizzo());
        statement.setString(7,element.getEmailIndirizzo());
        statement.setString(8,element.getIndirizzo());
        statement.setString(9,element.getCAPIndirizzo());
        statement.setInt(10,element.getId());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public synchronized List<OrdineBean> doRetrieveOnCondition(Condition<OrdineBean> cond) throws SQLException {
        List<OrdineBean> ordini = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from ordine");

        while(set.next()) {
            int id = set.getInt("id");
            Date dataAcquisto = set.getDate("data_acquisto");
            double prezzoTotale = set.getDouble("prezzo_totale");
            boolean spedizioneRapida = set.getBoolean("spedizione_rapida");
            int cliente = set.getInt("utente");
            String nomeIndirizzo = set.getString("nome_indirizzo");
            String emailIndirizzo = set.getString("email_indirizzo");
            String indirizzo = set.getString("indirizzo");
            String cittaIndirizzo = set.getString("citta_indirizzo");
            String CAPIndirizzo = set.getString("CAP_indirizzo");
            OrdineBean ordine = new OrdineBean(id, dataAcquisto, prezzoTotale, spedizioneRapida, cliente, nomeIndirizzo,emailIndirizzo,indirizzo,cittaIndirizzo,CAPIndirizzo);

            if(cond.test(ordine))
                ordini.add(ordine);
        }

        statement.close();
        conn.close();
        return ordini;
    }

    @Override
    public synchronized List<OrdineBean> doRetrieveAll() throws SQLException {
        List<OrdineBean> ordini = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from ordine");

        while(set.next()) {
            int id = set.getInt("id");
            Date dataAcquisto = set.getDate("data_acquisto");
            double prezzoTotale = set.getDouble("prezzo_totale");
            boolean spedizioneRapida = set.getBoolean("spedizione_rapida");
            int cliente = set.getInt("utente");
            String nomeIndirizzo = set.getString("nome_indirizzo");
            String emailIndirizzo = set.getString("email_indirizzo");
            String indirizzo = set.getString("indirizzo");
            String cittaIndirizzo = set.getString("citta_indirizzo");
            String CAPIndirizzo = set.getString("CAP_indirizzo");
            OrdineBean ordine = new OrdineBean(id, dataAcquisto, prezzoTotale, spedizioneRapida, cliente, nomeIndirizzo,emailIndirizzo,indirizzo,cittaIndirizzo,CAPIndirizzo);
        }

        statement.close();
        conn.close();
        return ordini;
    }
}
