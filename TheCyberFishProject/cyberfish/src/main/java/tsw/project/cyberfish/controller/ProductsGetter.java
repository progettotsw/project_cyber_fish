package tsw.project.cyberfish.controller;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import tsw.project.cyberfish.model.ProdottoBean;
import tsw.project.cyberfish.model.ProdottoDAO;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@WebServlet(name = "ProductsGetter", value = "/ProductsGetter")
public class ProductsGetter extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int type = Integer.parseInt(request.getParameter("type"));
        int top = Integer.parseInt(request.getParameter("top"));
        ProdottoDAO retriever = new ProdottoDAO();
        List<ProdottoBean> products;

        try {
            if(type == 0){
                products = retriever.doRetrieveAll();
            }
            else{
                int pesce = Integer.parseInt(request.getParameter("pesce"));
                int molluschi = Integer.parseInt(request.getParameter("molluschi"));
                int crostacei = Integer.parseInt((request.getParameter("crostacei")));
                products = retriever.doRetrieveOnCondition(prod -> prod.getTipo() == pesce || prod.getTipo() == molluschi || prod.getTipo() == crostacei);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        int productsSize;
        if(top == 0)
        {
            productsSize = products.size();
        }
        else
        {
            productsSize = top;
            products.sort((a,b) -> b.getNumeroOrdini()-a.getNumeroOrdini());

        }
        String res = "[";

        for(int i = 0; i < productsSize; i++)
        {
            if(i == 0)
            {
                res += products.get(i).stringify();
            } else
            {
                res += "," + products.get(i).stringify();
            }
        }
        res += "]";
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.println(res);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
